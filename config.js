module.exports = {
    //MongoDB
    MONGO_URL: process.env.MONGO_URL || 'mongodb://localhost:27017/e-shop',
    
    //JWT
    TOKEN_SECRET: process.env.TOKEN_SECRET || 'pvpnCCZfwOF85pBjbOebZiYIDhZ3w9LZrKwBZ7152K89mPCOHtbRlmr5Z91ci4L',

    //Express Server Port
    LISTEN_PORT: process.env.LISTEN_PORT || 5000
};