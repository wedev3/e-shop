const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const Router = require('./src/routes/route.js');
var multer = require('multer');
var path = require('path');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './src/assets/uploads')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname))
    }
})
var upload = multer({ storage: storage })

const app = express();

// for parsing application/json
app.use(express.json());

// for parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true })); 

// for parsing multipart/form-data
app.use(upload.any()); 
app.use(express.static('public'));

//read upload image => baseurl/uploads/imgname.extension
app.use('/uploads', express.static('src/assets/uploads'))


app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept, **Authorization**, authorization, access-control-allow-origin, access-control-allow-methods");

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    
    // Pass to next layer of middleware
    next();
});


mongoose.connect(config.MONGO_URL,
    {
        useNewUrlParser: true,
        //useUnifiedTopology: true
    }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
    console.log("Connected successfully");
});


//app.use(Router);
require('./src/routes/route')(app);

app.listen(config.LISTEN_PORT, () => {
    console.log("Server is running at port ", config.LISTEN_PORT);
});