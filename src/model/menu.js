const mongoose = require("mongoose");

const MenuSchema = new mongoose.Schema({
    name : {
        type: String,
        require: true,
        trim: true,
    },
    icon: {
        type: String,
        require: false,
        trim: true
    },
    url: {
        type: String,
        require: false,
        trim: true
    },
    desc: {
        type: String,
        require:false,
        trim: true
    }
});

const Menu = mongoose.model("menu",MenuSchema);
module.exports = Menu;