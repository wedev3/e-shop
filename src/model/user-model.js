const mongoose = require("mongoose");
const validator = require('validator');
const mongoosePaginate = require('mongoose-paginate');
const bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

const UserSchema = new Schema({
    role: {
        type: Schema.Types.ObjectId,
        ref: 'roles'
    },

    first_name: {
        type: String,
        required: true,
        trim: true
    },
    last_name: {
        type: String,
        require: true,
        trim: true
    },
    gender: {
        type: String,
        require: true
    },

    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        unique: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        trim: true,
        validate(value) {
            if (value.toLowerCase().includes('password')) {
                throw new Error('Password cannot contain "password"')
            }
        }
    },
    pob: {
        type: String,
        require: false
    },
    dob: {
        type: String,
        require: false
    },
    tel: {
        type: String,
        require: false
    },
    phone_code: {
        type: String,
        require: true
    },
    address: {
        type: String,
        require: false
    },
    profile: {
        type: String,
        require: false
    },
    status: {
        type: Boolean,
        require: false
    },
    desc: {
        type: String,
        require: false
    },

    created_at :{
        type:Date,
        require: false
    },

    updated_at :{
        type:Date,
        require: false
    }
    

});

const User = mongoose.model("users", UserSchema);

module.exports = User;