
const mongoose = require('mongoose');

const OrdersSchema = new mongoose.Schema({
    order_code : {
        type: String,
        require: true,
    },
    user_id : {
        type: String,
        require: true
    },
    shippingAddress_id : {
        type: String,
        require: true
    },
    totalPrice: {
        type: Number,
        require: false
    },
    tax: {
        type: Number,
        require: false
    },
    dateCreated: {
        type: Date,
        require:false
    },
    dateUpdated:{
        type: Date,
        require:false
    }
});

const orders = new mongoose.model("orders", OrdersSchema);
module.exports = orders;