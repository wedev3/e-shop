const mongoose = require('mongoose');
const { stringify } = require('postcss');
var Schema = mongoose.Schema;

const ProductSchema = new mongoose.Schema({

    product_category: {
        type: Schema.Types.ObjectId,
        ref: 'product_categories'
    },
    product_brand: {   
        type: Schema.Types.ObjectId,
        ref: 'product_brands'
    },

    product_supplier: {
        type: Schema.Types.ObjectId,
        ref: 'product_suppliers'
    },

    code : {
        type: String,
        require: true,
        unique: true
    },
    name : {
        type: String,
        require: true
    },
    price : {
        type: Number,
        require: false
    },
    qty: {
        type: Number,
        require: false
    },
    countInStock: { 
        type: Number,
        require: false
    },
    
    status:{
        type:Boolean,
        require:false,
    },

    created_at :{
        type:Date,
        require: false
    },

    updated_at :{
        type:Date,
        require: false
    }
       
});

const products = new mongoose.model("products",ProductSchema);
module.exports = products;