
const mongoose = require('mongoose');

const ProductBrandSchema = new mongoose.Schema({
    name : {
        type: String,
        require: true,
    },
    icon : {
        type: String,
        require: false
    },
    description : {
        type: String,
        require: false
    },
    status: {
        type: Boolean,
        require: false
    },
    created_at :{
        type:Date,
        require: false
    },

    updated_at :{
        type:Date,
        require: false
    }
});

const proBrand = new mongoose.model("product_brands",ProductBrandSchema);
module.exports = proBrand;