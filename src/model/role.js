const mongoose = require("mongoose");

const RoleSchema = new mongoose.Schema({

    title: {
        type: String,
        required: true,
        trim: true
    },

    desc: {
        type: String,
        required: false,
        trim: true
    }
});

const Role = mongoose.model("roles", RoleSchema);

module.exports = Role;

