const mongoose = require('mongoose');


const SupplierSchema = new mongoose.Schema({
    name : {
        type: String,
        require: true,
    },
    email : {
        type: String,
        require: false,
    },
    address : {
        type: String,
        require: false
    },
    city: {
        type: String,
        require: false
    },
    country: {
        type: String,
        require: false
    },
    tel:{
        type: String,
        require: false
    },
    zip_code:{
        type:String,
        require:false
    },
    created_at :{
        type:Date,
        require: false
    },

    updated_at :{
        type:Date,
        require: false
    }
});

const proSupplier = new mongoose.model("product_suppliers",SupplierSchema);
module.exports = proSupplier;