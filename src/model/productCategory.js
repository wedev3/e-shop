const mongoose = require('mongoose');

const ProductCategorySchema = new mongoose.Schema({
    name : {
        type: String,
        require: true,
    },
    icon : {
        type: String,
        require: false
    },
    description : {
        type: String,
        require: false
    },
    status: {
        type: Boolean,
        require: false
    },
    created_at :{
        type:Date,
        require: false
    },

    updated_at :{
        type:Date,
        require: false
    }
});

const proCategory = new mongoose.model("product_categories",ProductCategorySchema);
module.exports = proCategory;