import RegisterForm from 'components/Auth/RegisterForm';

export default function Register() {
    return (
        <>
            <div className="-mt-36">
                <RegisterForm />
            </div>
        </>
    );
}