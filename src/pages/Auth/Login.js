import LoginForm from 'components/Auth/LoginForm';

export default function Login() {
    return (
        <>
            <div className="-mt-36">
                <LoginForm />
            </div>
        </>
    );
}
