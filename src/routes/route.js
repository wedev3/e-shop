const authController = require('../controllers/authController');
const userController = require('../controllers/userController');
const roleController = require('../controllers/RoleController');
const menuController = require('../controllers/MenuController');
const proBrandController = require('../controllers/ProductBrandController');
const proCategoryController = require('../controllers/ProductCategoryController');
const proSupplierController = require('../controllers/SupplierController');
const productsController = require('../controllers/ProductController');
const ordersController = require('../controllers/OrderController');

const config = require('../../config');
const jwt = require('jwt-simple');
const moment = require('moment');
const userModel = require('../model/user-model');
//const roleModel = require('../model/role');
const { EndOfLineState } = require('typescript');


//Authenticiation Middleware
function ensureAthenticated(req, res, next)  {
    if(!req.headers.authorization) {
        return res.status(401).send({success: false, messsage: 'Missing token!'});
    }
    var token = req.headers.authorization.split(' ')[1];
    var payload = null;

    
    try {
        payload = jwt.decode(token, config.TOKEN_SECRET);
    } catch (err) {
        return res.status(401).send({success: false, messsage: 'Invalid token!'});
    }
    
    if(payload.exp <= moment().unix()) {
        return res.status(401).send({success: false, message: 'Token expired!'});
    }
    userModel.findOne({
        email: payload.email
    })
    .exec((err, user)=>{
        if(!user) {
            return res.status(400).send({success: false, message: 'User not found'});
        } else {
            req.user = payload.email;
            next();
        }
    })
}



//ROUTES
module.exports = function(app) {
    //AUTH
    app.post('/login', authController.login);

    //USER
    app.get('/users', ensureAthenticated, userController.listUser);
    app.get('/user/:id', ensureAthenticated, userController.showList);
    app.post('/user',ensureAthenticated, userController.createNew);
    app.patch('/user/:id', ensureAthenticated, userController.update);
    app.delete('/user/:id', ensureAthenticated, userController.delete);
    app.get('/search/:first_name', userController.searchUser);

    // Search User
    // app.get('/search/:name', async function(req,res){
    //     const regex = new RegExp(req.params.first_name, 'i');
    //     userModel.find({first_name:regex}).then((result) => {
    //         res.status(200).json(result)
    //     })
    
    // });

    // ROLE 
    app.get('/roles', ensureAthenticated, roleController.list);
    app.get('/role/:id',ensureAthenticated, roleController.showList);
    app.post('/role', ensureAthenticated , roleController.createNew);
    app.patch('/role/:id', ensureAthenticated , roleController.update);
    app.delete('/role/:id',ensureAthenticated , roleController.delete);

    // MENU
    app.get('/menus', ensureAthenticated,  menuController.listMenu);
    app.get('/menu/:id', ensureAthenticated,  menuController.showList);
    app.post('/menu', ensureAthenticated , menuController.createNew);
    app.patch('/menu/:id', ensureAthenticated, menuController.update);
    app.delete('/menu/:id', ensureAthenticated, menuController.delete);

    // PRODUCT BRAND
    app.get('/productBrands',ensureAthenticated , proBrandController.list);
    app.post('/productBrand',ensureAthenticated, proBrandController.createNew);
    app.patch('/productBrand/:id',ensureAthenticated , proBrandController.update);
    app.delete('/productBrand/:id',ensureAthenticated , proBrandController.delete);
    app.get('/productBrand/:id',ensureAthenticated , proBrandController.showList);

    // PRODUCT CATEGORY
    app.get('/productCategorys',ensureAthenticated , proCategoryController.list);
    app.post('/productCategory',ensureAthenticated , proCategoryController.createNew);
    app.patch('/productCategory/:id',ensureAthenticated , proCategoryController.update);
    app.delete('/productCategory/:id',ensureAthenticated, proCategoryController.delete);
    app.get('/productCategory/:id',ensureAthenticated, proCategoryController.showList);

   // PRODUCT SUPPLIER 
   app.get('/productSuppliers',ensureAthenticated, proSupplierController.list);
   app.post('/productSupplier',ensureAthenticated, proSupplierController.createNew);
   app.patch('/productSupplier/:id',ensureAthenticated, proSupplierController.update);
   app.delete('/productSupplier/:id',ensureAthenticated, proSupplierController.delete);
   app.get('/productSupplier/:id',ensureAthenticated, proSupplierController.showList);

   // PRODUCTS
   app.get('/products',ensureAthenticated, productsController.list);
   app.post('/product',ensureAthenticated, productsController.createNew);
   app.patch('/product/:id',ensureAthenticated, productsController.update);
   app.delete('/product/:id',ensureAthenticated, productsController.delete);
   app.get('/product/:id',ensureAthenticated, productsController.showList);

   // PRODUCT ORDERS
   app.get('/orders',ensureAthenticated, ordersController.list);
   app.post('/order',ensureAthenticated, ordersController.createNew);
   app.patch('/order/:id',ensureAthenticated, ordersController.update);
   app.delete('/order/:id',ensureAthenticated, ordersController.delete);
   app.get('/order/:id',ensureAthenticated, ordersController.showList);

};   

