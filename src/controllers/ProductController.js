
var Products = require('../model/products');

const {successRes, errorRes,successResWithPagination} = require('./responseHelper');

//Get role list
exports.list = async function(req, res) {
   
    var limit = Math.max(0, req.query.limit) 

    if(!limit) {
        limit = 10
    }
    var page = Math.max(0, req.query.page)
    
    if (!page) {
        res.status(401).send({success: false, message: 'page is required.'})
    }
    var skip = (page-1)*limit

   var query = await Products.find({}).populate(['product_category','product_brand','product_supplier']).skip(skip).limit(limit)
   
    const documentCount = await Products.countDocuments({});
    const mapped = query.map(element => ({
        _id:element._id,
        product_category: element.product_category,
        product_brand : element.product_brand,
        product_supplier : element.product_supplier,
        code: element.code,
        name: element.name,
        price: element.price,
        qty: element.qty,
        countInStock:element.countInStock,
        status: element.status,
        created_at: element.created_at,
        updated_at : element.updated_at
    }))

    try {
        
        successResWithPagination(res, "Successfully retreive Products", {currentPage: page, perPage: limit, totalCount: documentCount}, mapped)
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }
}

//Create Product Brand 
exports.createNew = async function(req, res) {
    var body = req.body;
    body['product_category'] = req.body.product_category, 
    body['product_brand'] = req.body.product_brand,
    body['product_supplier'] = req.body.product_supplier,
    body['created_at'] = Date.now();
    body['updated_at'] = Date.now();
    const products = new Products(body);

    try {
        await (await products.save()).populate(['product_category','product_brand','product_supplier']);
        successRes(res, "Create Products Succesfully!!!", products)
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Update Product Brand
exports.update = async function(req, res) {

    var body = req.body;
    body['product_category'] = req.body.product_category, 
    body['product_brand'] = req.body.product_brand,
    body['product_supplier'] = req.body.product_supplier,
    body['updated_at'] = Date.now();
    const products = await Products.findByIdAndUpdate(req.params.id, body,{new:true,runValidators:true})

    try{
        if(products){
            successRes(res, "Update Products Succesfully!!!", products)
        }
        res.send(products);
           
    }catch(err){
        //errorRes(res, err.message || 'Something went wrong');
        errorRes(res,"Products ID Not Found!!!!")
    }

}

//Delete ProductBrand
exports.delete = async function(req, res) {
    const products = await Products.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete Products Successfully!!",products)
        res.send(products)
      }catch(err){
          errorRes(res,err.message || 'Somethin went wrong')
      }
}

//Show ProductBrand
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await Products.findById(_id).then((products)=> {
        successRes(res, "Successfully retreive Products", products);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "Products ID Not Found !!!!")

    })
    
}