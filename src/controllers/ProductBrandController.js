
var productBrand = require('../model/productBrand');
const {successRes, errorRes} = require('./responseHelper');

//Get role list
exports.list = async function(req, res) {
    const proBrand = await productBrand.find({}).sort({title:1});
    
    try {
        successRes(res, "Successfully retreive Product Brand", proBrand);
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }
}

//Create Product Brand
exports.createNew = async function(req, res) {

    var body = req.body;
    body['created_at'] = Date.now();
    body['updated_at'] = Date.now();

    const proBrand = new productBrand(body);
    try {
        await proBrand.save();
        successRes(res, "Create ProductBrand Succesfully!!!", proBrand)
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Update Product Brand
exports.update = async function(req, res) {

    var body = req.body;
    body['updated_at'] = Date.now();

    const proBrandID = await productBrand.findByIdAndUpdate(req.params.id,body,{new:true,runValidators:true})

    try{
        if(proBrandID){
            successRes(res, "Update Product Brand Succesfully!!!", proBrandID)
        }
        res.send(proBrandID);
           
    }catch(err){
        //errorRes(res, err.message || 'Something went wrong');
        errorRes(res,"ProductBrand ID Not Found!!!!")
    }

}

//Delete ProductBrand
exports.delete = async function(req, res) {
    const proBrand = await productBrand.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete ProductBrand Successfully!!",proBrand)
        res.send(proBrand)
      }catch(err){
          errorRes(res,err.message || 'Somethin went wrong')
      }
}

//Show ProductBrand
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await productBrand.findById(_id).then((proBrand)=> {
        successRes(res, "Successfully retreive ProductBrand", proBrand);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "ProductBrand ID Not Found !!!!")

    })
    
}