
var productCategory = require('../model/productCategory');
const {successRes, errorRes} = require('./responseHelper');

//Get role list
exports.list = async function(req, res) {
    const proCategory = await productCategory.find({}).sort({title:1});
    
    try {
        successRes(res, "Successfully retreive Product Category", proCategory);
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }
}

//Create Product Brand
exports.createNew = async function(req, res) {
    var body = req.body;
    body['created_at'] = Date.now();
    body['updated_at'] = Date.now();
    const proCategory = new productCategory(body);
    try {
        await proCategory.save();
        successRes(res, "Create ProductBrand Succesfully!!!", proCategory)
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Update Product Brand
exports.update = async function(req, res) {

    var body = req.body;
    body['updated_at'] = Date.now();
    const proCateID = await productCategory.findByIdAndUpdate(req.params.id,body,{new:true,runValidators:true})

    try{
        if(proCateID){
            successRes(res, "Update Product Category Succesfully!!!", proCateID)
        }
        res.send(proCateID);
           
    }catch(err){
        //errorRes(res, err.message || 'Something went wrong');
        errorRes(res,"Product Category ID Not Found!!!!")
    }

}

//Delete ProductBrand
exports.delete = async function(req, res) {
    const proCategory = await productCategory.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete Product Category Successfully!!",proCategory)
        res.send(proCategory)
      }catch(err){
          errorRes(res,err.message || 'Somethin went wrong')
      }
}

//Show ProductBrand
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await productCategory.findById(_id).then((proCategory)=> {
        successRes(res, "Successfully retreive Product Category", proCategory);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "Product Category ID Not Found !!!!")

    })
    
}