var roleModel = require('../model/role');
const {successRes, errorRes} = require('./responseHelper');

//Get role list
exports.list = async function(req, res) {
    const roles = await roleModel.find({}).sort({title:1});
    
    try {
        successRes(res, "Successfully retreive roles", roles);
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }
}

//Create new role
exports.createNew = async function(req, res) {
    const role = new roleModel(req.body);
    try {
        await role.save();
        successRes(res, "Create Role Succesfully!!!", role)
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Show Role
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await roleModel.findById(_id).then((role)=> {
        successRes(res, "Successfully retreive role", role);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "Role ID Not Found !!!!")

    })
    
}

//Update Role
exports.update = async function(req, res) {
    const role_id = await roleModel.findByIdAndUpdate(req.params.id, req.body,{new:true,runValidators:true})

    try{
        if(role_id){
            successRes(res, "Update Role Succesfully!!!", role_id)
        }
        res.send(role_id);
           
    }catch(err){
        //errorRes(res, err.message || 'Something went wrong');
        errorRes(res,"Role ID Not Found!!!!")
    }

}


//Delete Role
exports.delete = async function(req, res) {
    const role = await roleModel.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete Role Successfully!!",role)
        res.send(role)
      }catch(err){
          errorRes(res,err.message || 'Somethin went wrong')
      }
}