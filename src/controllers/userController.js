var userModel = require('../model/user-model');
const {successRes, errorRes,successResWithPagination} = require('./responseHelper');
const bcrypt = require('bcryptjs');
const { FaRegCaretSquareDown } = require('react-icons/fa');

//Get Users list with pagination
exports.listUser = async function(req, res) { 
    
    var limit = Math.max(0, req.query.limit)

    if(!limit) {
        limit = 10
    }
    var page = Math.max(0, req.query.page)
    
    if (!page) {
        res.status(401).send({success: false, message: 'page is required.'})
    }
    var skip = (page-1)*limit

    var query = await userModel.find({}).populate('role').skip(skip).limit(limit)
    const mapped = query.map(element => ({
        _id:element._id,
        role: element.role ,
        first_name: element.first_name,
        last_name: element.last_name,
        gender: element.gender,
        email: element.email,
    //    password: element.password,
        pob:element.pob,
        dob:element.dob,
        tel:element.tel,
        address:element.address,
        status:element.status,
        desc:element.desc,
        profile: req.protocol + "://" + req.get('host')+'/uploads/'+element.profile,
        created_at: element.created_at,
        updated_at : element.updated_at
    }))
    const documentCount = await userModel.countDocuments({});
    try {
        successResWithPagination(res, "Successfully retreive users", {currentPage: page, perPage: limit, totalCount: documentCount}, mapped)
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }

}

//Create new user
exports.createNew =  async function(req, res) {
    let body = {
        role: req.body.role ,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        gender: req.body.gender,
        email: req.body.email,
        password: await bcrypt.hash(req.body.password, 10),
        pob:req.body.pob,
        dob:req.body.dob,
        tel:req.body.tel,
        address:req.body.address,
        status:req.body.status,
        desc:req.body.desc,
        created_at: Date.now(),
        updated_at: Date.now()
      
    }
   
    // Chec file upload
    if (req.files[0]) {
        body['profile'] = req.files[0].filename
    }
    
    const user = new userModel(body);
    try {
        await (await user.save()).populate('role');
        successRes(res, "Create User Succesfully!!!", user);
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Show user
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await userModel.findById(_id).then((user)=> {
        successRes(res, "Successfully retreive user", user);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "User ID Not Found !!!!")

    })
    
}

//Update User
exports.update = async function(req, res) {
   
    let body = {
        role: req.body.role,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        gender: req.body.gender,
        email: req.body.email,
        password: req.body.password,
        pob:req.body.pob,
        dob:req.body.dob,
        tel:req.body.tel,
        address:req.body.address,
        status:req.body.status,
        updated_at : Date.now()
    }

    if (req.files[0]) {
        body['profile'] = req.files[0].filename
    }

  
    const user_id = await userModel.findByIdAndUpdate(req.params.id, body,{new:true,runValidators:true})
    try{
        if(user_id){
            successRes(res, "Update User Succesfully!!!", user_id)
        } 
    }catch(err){
        errorRes(res,"User ID Not Found!!!!")
    }

}
// Search User
exports.searchUser = async function(req,res){

    const searchField = req.params.first_name;
    
        userModel.find({first_name:{$regex: searchField, $options: '$i'}})
        .then(data =>{
        successRes(res,"Search User Successfully!!",data)
        res.send(data);
        })
        .catch((err) => {
            //errorRes(res,err.message || 'User Not Found!!!')
            console.log(err)
        })  
}

//Delete user
exports.delete = async function(req, res) {
    const user = await userModel.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete User Successfully!!",user)
        res.send(user)
    }catch(err){
          errorRes(res,err.message || 'Somethin went wrong')
    }
}