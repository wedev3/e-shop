
var productSupplier = require('../model/supplier');
const {successRes, errorRes} = require('./responseHelper');

//Get role list
exports.list = async function(req, res) {
    const proSupplier = await productSupplier.find({}).sort({title:1});
    
    try {
        successRes(res, "Successfully retreive Product Supplier", proSupplier);
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }
}

//Create Product Brand
exports.createNew = async function(req, res) {

    var body = req.body;
    body['created_at'] = Date.now();
    body['updated_at'] = Date.now();

    const proSupplier = new productSupplier(body);
    try {
        await proSupplier.save();
        successRes(res, "Create Product Supplier Succesfully!!!", proSupplier)
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Update Product Brand
exports.update = async function(req, res) {

    var body = req.body;
    body['updated_at'] = Date.now();
    const proSupplier = await productSupplier.findByIdAndUpdate(req.params.id,body,{new:true,runValidators:true})

    try{
        if(proSupplier){
            successRes(res, "Update Product Supplier Succesfully!!!", proSupplier)
        }
        res.send(proSupplier);
           
    }catch(err){
        //errorRes(res, err.message || 'Something went wrong');
        errorRes(res,"Product Supplier ID Not Found!!!!")
    }

}

//Delete ProductBrand
exports.delete = async function(req, res) {
    const proSupplier = await productSupplier.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete Product Supplier Successfully!!",proSupplier)
        res.send(proSupplier)
      }catch(err){
          errorRes(res,err.message || 'Somethin went wrong')
      }
}

//Show ProductBrand
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await productSupplier.findById(_id).then((proSupplier)=> {
        successRes(res, "Successfully retreive Product Supplier", proSupplier);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "Product Supplier ID Not Found !!!!")

    })
    
}