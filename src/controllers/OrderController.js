
var Orders = require('../model/orders');
const {successRes, errorRes} = require('./responseHelper');

//Get Product Orders list
exports.list = async function(req, res) {
    const orders = await Orders.find({}).sort({title:1});
    
    try {
        successRes(res, "Successfully retreive Products Orders", orders);
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }
}

//Create Product Orders
exports.createNew = async function(req, res) {
    const orders = new Orders(req.body);
    try {
        await orders.save();
        successRes(res, "Create Product Orders Succesfully!!!", orders)
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Update Product Orders
// exports.update = async function(req, res) {
//     const order_id = await Orders.findByIdAndUpdate(req.params.id, req.body,{new:true,runValidators:true})

//     try{
//         if(order_id){
//             successRes(res, "Update Product Orders Succesfully!!!", order_id)
//         }
//         res.send(order_id);
           
//     }catch(err){
//         //errorRes(res, err.message || 'Something went wrong');
//         errorRes(err.message,"Product Orders ID Not Found!!!!")
//     }

// }

// Update a note identified by the noteId in the request
exports.update = (req, res) => {

    // Validate Request
    // if(!req.body.content) {
    //     return res.status(400).send({
    //         message: "Note content can not be empty"
    //     });
    // }

    // Find note and update it with the request body
    const order = Orders.findByIdAndUpdate(req.params.id, {
        order_code: req.body.order_code || "Untitled Note",
        user_id: req.body.user_id,
        shippingAddress_id: req.body.shippingAddress_id,
        totalPrice: req.body.totalPrice,
        tax:req.body.tax,
        dateCreated: req.body.dateCreated,
        dateUpdated: req.body.dateUpdated

    }, {new: true})
    .then(order => {
        if(!order) {
            return res.status(404).send({
                message: "Order not found with id " + req.params.id
            });
        }
        successRes(res, "Update Product Orders Succesfully!!!", order)
       // res.send(order);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Order not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error updating Order with id " + req.params.id
        });
    });
};

//Delete Product Orders
exports.delete = async function(req, res) {
    const orders = await Orders.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete Product Orders Successfully!!",orders)
        res.send(orders)
      }catch(err){
           errorRes(res, "Product Orders ID Not Found !!!!")
      }
}

//Show Product Orders
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await Orders.findById(_id).then((orders)=> {
        successRes(res, "Successfully retreive Product Orders", orders);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "Product Orders ID Not Found !!!!")

    })
    
} 