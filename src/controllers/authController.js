var userModel = require('../model/user-model');
const {successRes, errorRes} = require('./responseHelper');
const bcrypt = require('bcryptjs');
const jwt = require('jwt-simple');
const config = require('../../config');

//import {add} from '../helpers/test'

exports.login = async function(req, res) {
    //console.log("======== Test helpers");
    //console.log(add(3, 4));

    if (!req.body.email) {
        return res.status(400).send({success: false, message: 'email require'});
    }

    if (!req.body.password) {
        return res.status(400).send({success: false, message: 'password require'});
    }

    userModel.findOne({
        email: req.body.email
    })
    .exec((err, user) => {
        if(err) {
            return res.status(500).send({success: false, message: err.message});
        }
        if(!user) {
            return res.status(400).send({success: false, message: 'User not found!'});
        }

        bcrypt.compare(req.body.password, user.password, function(err, success) {
            if (err) {
                return res.status(400).send({success: false, message: err.message});
            }
            if (success) {
                var token = jwt.encode(req.body, config.TOKEN_SECRET);
                return res.status(200).send({success: true, message: 'Successfully login', data: {
                    id: user.id,
                    email: user.email,
                    token: token
                }})
            } else {
                return res.status(400).send({success: false, message: 'Invalid password'});
            }
        });

        /*bcrypt.hash(req.body.password, 10, function(err, hash) {
            if (err) {
                return res.status(400).send({success: false, message: err.message});
            }
            bcrypt.compare(user.password, hash, function(err, success) {
                if (err) {
                    return res.status(400).send({success: false, message: err.message});
                }
                if (success) {
                    var token = jwt.encode(req.body, config.TOKEN_SECRET);
                    return res.status(200).send({success: true, message: 'Successfully login', data: {
                        id: user.id,
                        email: user.email,
                        token: token
                    }})
                } else {
                    return res.status(400).send({success: false, message: 'Invalid password'});
                }
            });
        })  */  
    });
}