exports.successRes = (res, message, data) => {
    return res.status(200).send({success:true, message: message, data: data});
}

exports.errorRes = (res, message) => {
    return res.status(500).send({success:false, message: message});
};

exports.successResWithPagination = (res, message, pagination, data) => {
    return res.status(200).send({
        success: true, 
        message: message, 
        pagination: {
            currentPage: pagination.currentPage, perPage: pagination.perPage, totalCount: pagination.totalCount
        },
        data: data
    })
}

exports.limit = () => {return 10}