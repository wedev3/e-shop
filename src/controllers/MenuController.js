var menuModel = require('../model/menu');
const {successRes, errorRes, successResWithPagination} = require('./responseHelper');
const StringDecoder = require('string_decoder').StringDecoder

//Get menu list with pagination
exports.listMenu = async function(req, res) {
    var limit = Math.max(0, req.query.limit)

    if(!limit) {
        limit = 10
    }
    var page = Math.max(0, req.query.page)
    
    if (!page) {
        res.status(401).send({success: false, message: 'page is required.'})
    }
    var skip = (page-1)*limit
    var query = await menuModel.find({}).skip(skip).limit(limit)
    const mapped = query.map(element => ({
        _id: element._id,
        name: element.name,
        desc: element.desc,
        url: element.url,
        icon: req.protocol + "://" + req.get('host')+'/uploads/'+element.icon
    }))
    const documentCount = await menuModel.countDocuments({});
    try {
        successResWithPagination(res, "Successfully retreive menus", {currentPage: page, perPage: limit, totalCount: documentCount}, mapped)
    }catch (error) {
        errorRes(res, error.message || "Some error occurred while retrieving notes.");
    }

}

//Create new Menu
exports.createNew = async function(req, res) {
    let body = {
        name: req.body.name,
        url: req.body.url,
        desc: req.body.desc
    }
    if (req.files[0]) {
        body['icon'] = req.files[0].filename
    }
    //return res.status(200).send({success:true, message: msg});
    const menu = new menuModel(body);
    try {
        await menu.save();
        successRes(res, "Create Menu Succesfully!!!", menu)
    } catch (error) {
        errorRes(res, error.message || 'Something went wrong');
    }
}

//Show Menu
exports.showList = async function(req, res) {
    const _id = req.params.id;
    await menuModel.findById(_id).then((menu)=> {
        successRes(res, "Successfully retreive Menu", menu);
    }).catch((error) => {
       // errorRes(res, error.message || "Some error occurred while retrieving notes.");
        errorRes(res, "Menu ID Not Found !!!!")
    })
    
}

//Update Menu ===
exports.update = async function(req, res) {
    let body = {
        name: req.body.name,
        url: req.body.url,
        desc: req.body.desc
    }
    if (req.files[0]) {
        body['icon'] = req.files[0].filename
    }

    const menu_id = await menuModel.findByIdAndUpdate(req.params.id, body,{new:true,runValidators:true})
    try{
        if(menu_id){
            successRes(res, "Update Menu Succesfully!!!", menu_id)
        } 
    }catch(err){
        errorRes(res,"Menu ID Not Found!!!!")
    }

}

//Delete Role
exports.delete = async function(req, res) {
    const menu = await menuModel.findByIdAndDelete(req.params.id)
    try{
        successRes(res,"Delete Menu Successfully!!",menu)
        res.send(menu)
      }catch(err){
          errorRes(res,err.message || 'Somethin went wrong')
      }
}