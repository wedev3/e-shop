import { BrowserRouter, Routes, Route, Navigate, useNavigate } from 'react-router-dom';
import Sidebar from 'components/Sidebar';
import Dashboard from 'pages/Dashboard';
import Settings from 'pages/Settings';
import Tables from 'pages/Tables';
import Maps from 'pages/Maps';
import Footer from 'components/Footer';
import Login from 'components/Auth/LoginForm';
import Register from 'components/Auth/RegisterForm';
import CreateUser from 'pages/CreateUserPage';
import EditeUser from 'pages/EditUserPage';


import Users from 'pages/Users';
import Menus from 'pages/Menu/MenuListPage';
import Products from 'pages/Products/ProductPage';

// Tailwind CSS Style Sheet
import 'assets/styles/tailwind.css';
import React, { useEffect } from 'react';
import { count } from 'model/user-model';
const isLoggedIn = require('./components/helper').default.isLoggedIn;

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menuList: [
                {
                    'name': 'Dashboard',
                    'url': '/',
                    'icon': 'dashboard',
                    'desc': 'some desc'
                },
                {
                    'name': 'Users',
                    'url': '/users',
                    'icon': 'group',
                    'desc': 'some desc'
                },
                {
                    'name': 'Settings',
                    'url': '/settings',
                    'icon': 'settings',
                    'desc': 'some desc'
                },
                {
                    'name': 'Tables',
                    'url': '/tables',
                    'icon': 'toc',
                    'desc': 'some desc'
                },
                {
                    'name': 'Products',
                    'url': '/products',
                    'icon': 'devices',
                    'desc': 'some desc'
                },
                {
                    'name': 'Product Categories',
                    'url': '/product/categories',
                    'icon': 'category',
                    'desc': 'some desc'
                },
                {
                    'name': 'Menus',
                    'url': '/menus',
                    'icon': 'dashboard',
                    'desc': 'some desc'
                }

            ]
        }
    }

    render() {
        const PrivateRoute = ({children}) => {
            return isLoggedIn ? children : <Navigate to="/login" />;
         }

         const GetSidebar = () => {
             return isLoggedIn ? <Sidebar menuList={this.state.menuList} /> : <div></div>;
         }

         const GetFooter = () => {
             return isLoggedIn ? <Footer/> : <div></div>;
         }

         const LoginRoute = () => {
             return isLoggedIn ? <Navigate to="/" /> : <Login/>;
         }

        return (
            <>                
                <GetSidebar/>
                <div className="md:ml-64">
                    <Routes>
                        <Route path="/register" element={<Register/>} />
                        <Route 
                            path="/login"
                            element={
                                <LoginRoute/>
                            }
                        />
                        <Route path="/" 
                            element={
                                <PrivateRoute><Dashboard/></PrivateRoute>
                            }
                        />
                        <Route path="/users" 
                            element={
                                <PrivateRoute><Users/></PrivateRoute>
                            }
                        />
                        <Route path="/user/create" 
                            element={
                                <PrivateRoute><CreateUser/></PrivateRoute>
                            }
                        />

                        <Route path="/user/edit" 
                            element={
                                <PrivateRoute><EditeUser/></PrivateRoute>
                            }
                        />


                        <Route path="/menus" 
                            element={
                                <PrivateRoute><Menus/></PrivateRoute>
                            }
                        />
                        <Route path="/settings" 
                            element={
                                <PrivateRoute><Settings/></PrivateRoute>
                            }
                        />
                        <Route path="/tables" 
                            element={
                                <PrivateRoute><Tables/></PrivateRoute>
                            }
                        />
                        <Route path="/products" 
                            element={
                                <PrivateRoute><Products/></PrivateRoute>
                            }
                        />
                        <Route path="/maps" 
                            element={
                                <PrivateRoute><Maps/></PrivateRoute>
                            }
                        />
                        <Route
                            path="*"
                            element={
                                <center><p>Page Not Found.</p></center>
                            }
                        />
                    </Routes>
                    <GetFooter/>
                </div>
            </>
        );
    }
}
