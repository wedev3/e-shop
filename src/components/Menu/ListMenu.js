import Card from '@material-tailwind/react/Card';
import CardHeader from '@material-tailwind/react/CardHeader';
import CardBody from '@material-tailwind/react/CardBody';
import Image from '@material-tailwind/react/Image';
import Progress from '@material-tailwind/react/Progress';
import Team1 from 'assets/img/team-1-800x800.jpg';
import Button from "@material-tailwind/react/Button";
import Icon from '@material-tailwind/react/Icon';
import React from 'react';
import CreateMenu from './CreateMenu';
import {FaArrowLeft, FaPlus} from 'react-icons/fa'
import Pagination from "@material-tailwind/react/Pagination";
import PaginationItem from "@material-tailwind/react/PaginationItem";

import ConfirmDeleteMenu from './ConfirmDeleteMenu';

import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { limit } from 'controllers/responseHelper';
import { sys } from 'typescript';
 


const helper = require('../helper').default;
const axios = require('axios').default

const List = Symbol('LIST_MENU');
const Create = Symbol('CREATE_MENU');
const Edit = Symbol('EDIT_MENU');

const PRE_PAGE = Symbol('PRE_PAGE');
const NUM_PAGE = Symbol('NUM_PAGE');
const NEXT_PAGE = Symbol('NEXT_PAGE');

const baseIconPath = '/src/assets/uploads/'

export default class MenuList extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {
            menus: [],
            mode: List,
            headerTitle: 'Menus',
            selectedMenu: null,
            isShowModal: false,
            
            currentPage: 1,
            page: 1,
            limit: 2,
            totalPage: 0
        }
    }

    componentDidMount() {
        this.fetchMenuList(this.state.page)
    }

    fetchMenuList = (page) => {
        axios({
            method: 'GET',
            url: helper.baseUrl+'/menus',
            headers: helper.headers,
            params: {page: page, limit: this.state.limit}
        }).then(res => {
            this.setState({menus: res.data.data});
            let totalCount = res.data.pagination.totalCount
            var _totalPage = totalCount/this.state.limit 
            _totalPage = parseInt(_totalPage)
            if(totalCount%this.state.limit>0){
                _totalPage += 1
            }
            this.setState({totalPage: _totalPage});
            console.log('totalpage = ', this.state.totalPage)
        }).catch(err => {
            console.log("error:",err.response.data.message);
            let errMsg = err.response.data.message
            NotificationManager.error(errMsg, 'Error');
        })
    }

    onClikAddMenu = () => {
        if (this.state.mode == List) {
            this.setState({mode: Create});
            this.setState({headerTitle: 'Create Menu'});
        } 
        if (this.state.mode == Create) {
            this.setState({mode: List});
            this.setState({headerTitle: 'Menus'});
        }
        if (this.state.mode == Edit) {
            this.setState({mode: List});
            this.setState({headerTitle: 'Menus'});
        }
    }

    onClickEditMenu = (menu) => {
        this.setState({mode: Edit});
        this.setState({headerTitle: 'Edit Menu'});
        this.setState({isShowModal: false});
        this.setState({selectedMenu: menu});
    }

    onClickCancel = (menu) => {
        this.setState({isShowModal: false});
    }
    onClickOk = (menu) => {
        axios({
            method: 'DELETE',
            url: helper.baseUrl+'/menu/'+menu._id,
            headers: helper.headers
        }).then(res => {
            console.log('res delete menu = ', res.data)
            let index = this.state.menus.indexOf(menu)
            this.state.menus.splice(index,1)
            this.setState({isShowModal: false});
            NotificationManager.success('Successfully delete menu.', 'Success');
        }).catch(err => {
            console.log("error:",err.response)
        })
    }

    onClickDeleteMenu = (menu) => {
        this.setState({selectedMenu: menu});
        this.setState({isShowModal: true});
    }

    didSaveMenuSuccess = () => {
        NotificationManager.success('Successfully saved menu.', 'Success');
        this.fetchMenuList();
        this.setState({mode: List});
    }

    onClickPageItem = (page, pageType) => {
        switch (pageType) {
            case PRE_PAGE:
                if(this.state.currentPage > 1) {
                    let _page = this.state.currentPage -= 1;
                    this.setState({currentPage: _page});
                    this.fetchMenuList(_page);
                } else {
                    NotificationManager.info('No page available', 'Info');
                }
            case NUM_PAGE:
                this.setState({currentPage: page})
                this.fetchMenuList(page);

            case NEXT_PAGE:
                if(this.state.currentPage < this.state.totalPage) {
                    let _page = this.state.currentPage += 1;
                    this.setState({currentPage: _page});
                    this.fetchMenuList(_page);
                } else {
                    NotificationManager.info('End of page.', 'Info');
                }
        }
        
    }
    
    
    render() {
        const MODAL = () => {
            if (this.state.isShowModal) {
                return (<ConfirmDeleteMenu 
                    isShowModal={true}
                    onClickCancel={this.onClickCancel}
                    onClickOk={this.onClickOk}
                    deleteData={this.state.selectedMenu}
                />)
            } else {
                return (<ConfirmDeleteMenu 
                    isShowModal={false}
                    onClickCancel={this.onClickCancel}
                    onClickOk={this.onClickOk}
                    deleteData={this.state.selectedMenu}
                />)
            }
        }

        const HEADERICON = () => {
            return this.state.mode == List ? (<FaPlus/>) : (<FaArrowLeft/>)
        }

        const BODY = () => {
            switch(this.state.mode) {
                case List: 
                    return (
                        <div className="overflow-x-auto">
                            <table className="items-center w-full bg-transparent border-collapse">
                                <thead>
                                    <tr>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Name
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Url
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Decription
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Icon
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {this.state.menus.map((menu, i) => (
                                        
                                        <tr key={i}> 
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                {menu.name}
                                            </th>
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                {menu.url}
                                            </th>
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                {menu.desc}
                                            </th>
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                <div className="flex">
                                                    <div className="w-10 h-10 rounded-full border-2 border-white">
                                                        <Image
                                                            src={menu.icon}
                                                            rounded
                                                            alt="..."
                                                        />
                                                    </div>
                                                </div>
                                            </th>
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                <div className="flex">
                                                <div className="flex-auto">
                                                        <Button color="cyan" ripple="light">
                                                            <Icon name="visibility" />
                                                        </Button>
                                                    </div>
                                                    <div className="flex-auto">
                                                        <Button color="teal" ripple="light"
                                                            onClick={()=>this.onClickEditMenu(menu)}
                                                        >
                                                            <Icon name="edit" />
                                                            
                                                        </Button>
                                                    </div>
                                                    <div className="flex-auto">
                                                        <Button color="red" ripple="light"
                                                            onClick={()=>this.onClickDeleteMenu(menu)}
                                                        >
                                                            <Icon name="delete" />
                                                        </Button>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            <_Pagination/>
                        </div>
                    )
                    case Create: return (<CreateMenu isEditMode={false} callbackMenuSaveSuccess={()=>this.didSaveMenuSuccess()}/>)
                    case Edit: return (<CreateMenu isEditMode={true} editData={this.state.selectedMenu} callbackMenuSaveSuccess={()=>this.didSaveMenuSuccess()}/>)
            }
        }

        const _Pagination = () => {
            var pageItems = []
            pageItems.push( 
                <PaginationItem href="#pre" ripple="dark" onClick = {()=>this.onClickPageItem(null,PRE_PAGE)}>
                    <Icon name="keyboard_arrow_left" />
                </PaginationItem>
            )
            for (let i = 0; i<this.state.totalPage; i++) {
                let _page = parseInt(i+1)
                let _url = "#"+_page
                pageItems.push( 
                    <PaginationItem 
                        color={this.state.currentPage === _page ? 'lightBlue' : ''} 
                        onClick = {() => this.onClickPageItem(_page, NUM_PAGE)}
                        href={_url} ripple="dark">
                        {_page}
                    </PaginationItem>
                )
            }
            pageItems.push( 
                <PaginationItem href="#last" ripple="dark" onClick = {()=>this.onClickPageItem(null, NEXT_PAGE)}>
                    <Icon name="keyboard_arrow_right" />
                </PaginationItem>
            )
            return (
                <Pagination>
                   {pageItems.map((item) =>
                        <div>{item}</div>
                   )}
                </Pagination>
            )
        }
        return (
            <div>
                <NotificationContainer/>
            <Card>
                <CardHeader color="blue" contentPosition="none">
                    <div className="w-full flex items-center justify-between">
                        <h2 className="text-white text-2xl">{this.state.headerTitle}</h2>
                        <Button
                                color="lightBlue"
                                buttonType="filled"
                                size="regular"
                                rounded={false}
                                block={false}
                                iconOnly={false}
                                ripple="light"
                                className="rounded-full h-16 w-16 align-top bg-light-blue-500"
                                onClick={this.onClikAddMenu}
                            >
                                <HEADERICON/>
                        </Button>
                    </div>
                </CardHeader>
                <CardBody>
                    <BODY/>
                </CardBody>
                <MODAL/>
            </Card>
            </div>
        );
    }
}
