import Card from '@material-tailwind/react/Card';
import CardHeader from '@material-tailwind/react/CardHeader';
import CardBody from '@material-tailwind/react/CardBody';
import Button from '@material-tailwind/react/Button';
import Input from '@material-tailwind/react/Input';
import Image from "@material-tailwind/react/Image";
import UploadImg from 'assets/img/upload.png';

import React from 'react';
import axios from 'axios';
import helper from '../helper';


export default class CreateMenu extends React.Component {
    constructor(props) {
        super(props)
        this.inputFileRef = React.createRef();
        this.state = {
            isEditMode: this.props.isEditMode,
            editData: this.props.editData,
            icon: null,
            fileSrc: null,
            errorName: null,
            errorUrl: null,
            name: null,
            url: null,
            desc: null
        }
    }

    componentDidMount() {
        if (this.state.isEditMode) {
            this.setState({name: this.state.editData.name});
            this.setState({url: this.state.editData.url});
            this.setState({desc: this.state.editData.desc});
            if (this.state.editData.icon) {
                this.setState({fileSrc: this.state.editData.icon})
            }
        }
    }

    callbackMenuSaveSuccess = () => {
        this.props.callbackMenuSaveSuccess()
    }

    onClickOnImage = () => {
        this.inputFileRef.current.click();
    }

    onChangeFileInput = (e) => {
        var file = e.target.files[0];
        this.setState({icon: file});
        var reader = new FileReader();
        reader.readAsDataURL(file);
        var _this = this
        reader.onload = function() {
            _this.setState({fileSrc: reader.result});
        }
    }

    onClickBtnSave = () => {
        if (this.state.name == null || this.state.name == "") {
            this.setState({errorName: "Name is required."});
        } else if (this.state.url == null || this.state.url == "") {
            this.setState({errorUrl: "Url is required."});
        } else {
            var body = {
                "name": this.state.name,
                "url": this.state.url,
                "files": this.state.icon,
                "desc": this.state.desc,
                "icon": this.state.icon
            }
            var formData = new FormData();
            Object.keys(body).forEach(function(key) {
                formData.append(key, body[key])
            });
            this.requestSave(formData)  
        }
    }

    requestSave = (body) => {
        var method = this.state.isEditMode ? "PATCH" : "POST"
        var suffix = this.state.isEditMode ? '/menu/'+this.state.editData._id : '/menu'
        axios({
            method: method,
            url: helper.baseUrl+suffix,
            headers: helper.headersFormData,
            data: body
        }).then(res => {
            console.log('res delete menu = ', res.data)
            this.callbackMenuSaveSuccess();
        }).catch(err => {
            console.log("error:",err)
        })
    }

    onChangeName = (e) => {
        this.setState({ name: e.target.value })
        this.setState({errorName: this.state.name ? null : 'Name is required.'});
    }

    onChangeUrl = (e) => {
        this.setState({ url: e.target.value });
        this.setState({errorUrl: this.state.url ? null : 'Url is required.'});
    }

    render() {
        
        return (
            <form>
                <h6 className="text-blue-500 text-sm mt-3 mb-6 font-light uppercase">
                    Enter Menu Information
                </h6>
                <div className="flex flex-wrap mt-10">
                    <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
                        <Input
                            type="text"
                            color="blue"
                            placeholder="Name"
                            value={this.state.name}
                            error={this.state.errorName}
                            onChange = {e => this.onChangeName(e)}
                        />
                    </div>
                    <div className="w-full lg:w-6/12 pl-4 mb-10 font-light">
                        <Input
                            type="text"
                            color="blue"
                            placeholder="Url"
                            value={this.state.url}
                            error={this.state.errorUrl}
                            onChange = {e => this.onChangeUrl(e)}
                        />
                    </div>
                    <div className="w-full lg:w-12/12 mb-10 font-light">
                        <Input
                            type="text"
                            color="blue"
                            placeholder="Description (Optional)"
                            value={this.state.desc}
                            onChange = {e => this.setState({ desc: e.target.value })}
                        />
                    </div>
                </div>

                <h6 className="text-blue-500 text-sm my-6 font-light uppercase">
                    Menu Icon
                </h6>
                <div className="flex flex-wrap mt-10">
                    <input type="file" 
                        ref={this.inputFileRef} 
                        onChange={(e)=>this.onChangeFileInput(e)} 
                        hidden
                    />
                    <div className="w-full lg:w-12/12 mb-10 font-light">
                        <Image
                            src={this.state.fileSrc ? this.state.fileSrc : UploadImg}
                            rounded={false}
                            raised={false}
                            alt="Rounded Image"
                            style={{width: '220px', height: '220px', cursor: 'pointer'}}
                            onClick = {this.onClickOnImage}
                        />
                    </div>
                </div>
                <div className="flex flex-wrap mt-10">
                    <Button color="lightBlue"
                        type = "button"
                        onClick={this.onClickBtnSave}
                    >
                        Save
                    </Button>
                </div>
            </form>
        )
    }
}
