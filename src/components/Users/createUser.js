
import React, { useState } from "react";

import Button from "@material-tailwind/react/Button";
import Input from '@material-tailwind/react/Input';
import Textarea from '@material-tailwind/react/Textarea';
import Image from "@material-tailwind/react/Image";
import UploadImg from 'assets/img/default-user-img.jpeg';
import DefaulfImg from 'assets/img/defaulf_photo.png';
import 'bootstrap';
import axios from 'axios';
import helper from '../helper';

import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import MobileDatePicker from "@mui/lab/MobileDatePicker";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';

import {NotificationManager} from 'react-notifications';

import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';

const moment = require('moment');

const List = Symbol('LIST_MENU');

//import AdapterDateFn from '@mui'

 
 class CreateUser extends React.Component { 
    
    constructor(props){
        super(props);

            this.inputFileRef = React.createRef();
            
            this.state = {
                value: null,
                isGregorian: true,

                isEditMode: this.props.isEditMode, 
                editData: this.props.editData,
                first_name: '',
                last_name: '',
                gender: '',
                email: '',
                password: null,
                confirm_password:null,
                pob: '',
                dob: null,
                tel: '',
                phoneCode: '',
                address : '',
                desc: '',
                profile: '',
                fileSrc: null,
                errorFirstName: null,
                errorLastName: null,
                errorEmail:null,
                errorPassword:null,
                errorConfirmPassword:null,    
                
                rolesList:[],
                selectedRoleId:null,
            };
        
    }
    componentDidMount(){

        this.fetchRoleList();

        if(this.state.isEditMode){
            this.setState({first_name:this.state.editData.first_name})
            this.setState({last_name:this.state.editData.last_name})
            this.setState({email:this.state.editData.email})
            this.setState({pob:this.state.editData.pob})
            this.setState({dob:this.state.editData.dob})
            this.setState({tel:this.state.editData.tel})
            this.setState({address:this.state.editData.address})
            this.setState({desc:this.state.editData.desc})   
            this.setState({gender: this.editData.gender})
            if (this.state.editData.profile) {
                this.setState({fileSrc: this.state.editData.profile})
            }     
        }
    };
    onChangeGender = e => {
        this.setState({ gender: e.target.value});
    }
    isValidEmail(email){
        let lastAtPos = this.state.email.lastIndexOf("@");
        let lastDotPos = this.state.email.lastIndexOf(".");
        if (
                !(
                    lastAtPos < lastDotPos &&
                    lastAtPos > 0 &&
                    this.state.email.indexOf("@@") == -1 &&
                    lastDotPos > 2 &&
                    this.state.email.length - lastDotPos > 2
                )
            ){
                return false;

            }else{
                return true;
            }
    }

    onClickBtnSave = () => {

        // Validation Feilds
        if(this.state.first_name == null || this.state.first_name == "") {
            this.setState({errorFirstName: "First Name is required"});
        }else if (this.state.last_name == null || this.state.last_name == "") {
            this.setState({errorLastName: "Last Name is required."});
        }else if(this.state.email == null || this.state.email == ''){
            this.setState({errorEmail: "Email is required"});
        }else if(!(this.isValidEmail(this.state.email))){
            this.setState({errorEmail: "Email is invalid"});
        }else if (this.state.gender == null || this.state.gender == '') {
            NotificationManager.error('Please select gender.', 'Error');
        }else{
            let body = {
                role: this.state.selectedRoleId,
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                gender: this.state.gender,
                email: this.state.email,
                address: this.state.address,
                pob: this.state.pob,
                dob: moment(this.state.dob).format('YYYY-MM-DD'),
                tel: this.state.tel.slice(this.state.phoneCode.length),
                phone_code: this.state.phoneCode,
                desc: this.state.desc,
                files: this.state.profile
            }
           
            if(!this.state.isEditMode){
                if(this.state.password == null || this.state.password == ''){
                    this.setState({errorPassword: "Password is required"});
                }else if(this.state.confirm_password == null || this.state.confirm_password == ''){
                    this.setState({errorConfirmPassword: "Confirm Password is required"});
                }else if(this.state.password !== this.state.confirm_password){
                    this.setState({errorConfirmPassword: "Password not match !!"});
                }else{
                    // check editMode is create set password value to that object 
                    if(!this.state.isEditMode){
                        body['password'] = this.state.password
                    }

                    var formData = new FormData();
                    Object.keys(body).forEach(function(key) {
                        formData.append(key, body[key])
                    });
                    this.requestSave(formData) 
                }
            }else{
                // check editMode is create set password value to that object 
                if(!this.state.isEditMode){
                    body['password'] = this.state.password
                }
                var formData = new FormData();
                Object.keys(body).forEach(function(key) {
                    formData.append(key, body[key])
                });
                this.requestSave(formData) 
            }  
        }
    }
    requestSave = (body) => {
        var method = this.state.isEditMode ? "PATCH" : "POST"
        var suffix = this.state.isEditMode ? '/user/'+this.state.editData._id : '/user'

        axios({
            method: method,
            url: helper.baseUrl+suffix,
            headers: helper.headersFormData,
            data: body
        }).then(res => {
            console.log('res users = ', res.data)
            this.callbackUserSaveSuccess();
        }).catch(err => {
            console.log("error:",err)
        })
    }

    onChangeFirstName = (e) => {
        this.setState({ first_name: e.target.value })
        this.setState({errorFirstName: this.state.first_name ? null : 'First Name is required.'});
    }
    onChangeLastName = (e) => {
        this.setState({ last_name: e.target.value })
        this.setState({errorLastName: this.state.last_name ? null : 'Last Name is required.'});
    }
    onChangeEmail = (e) => {
        this.setState({ email: e.target.value })
        this.setState({errorEmail: this.state.email ? null : 'Email is required.'});
    }
    onChangePassword = (e) => {
        this.setState({ password: e.target.value })
        this.setState({errorPassword: this.state.password ? null : 'Password is required.'});
    }
    onChangeConfirmPassword = (e) => {
        this.setState({ confirm_password: e.target.value })
        this.setState({errorConfirmPassword: this.state.confirm_password ? null : 'Confirm Password is required.'});
    }

    handleOnChangePhone(value, data, event, formattedValue) {
        this.setState({ tel: value})
        this.setState({ phoneCode: data.dialCode});
    }

    // callback
    callbackUserSaveSuccess = () => {
        this.props.callbackUserSaveSuccess()
    }


    // image 
    onClickOnImage = () => {
        this.inputFileRef.current.click();
    }

    onChangeFileInput = (e) => {
        var file = e.target.files[0];
        this.setState({profile: file});
        var reader = new FileReader();
        reader.readAsDataURL(file);
        var _this = this
        reader.onload = function() {
            _this.setState({fileSrc: reader.result});
        }
    }

     // Role list 
     fetchRoleList = () => {
        axios({
            method: 'GET',
            url: helper.baseUrl+'/roles',
            headers: helper.headers,
           
        }).then(res => {
            this.setState({rolesList: res.data.data});
            /*if(this.state.rolesList.length > 0){
                this.setState({ selectedRoleId: this.state.rolesList[0]._id});
            }*/
           
        }).catch(err => {
            console.log("error:",err.response.data.message);
            let errMsg = err.response.data.message
            NotificationManager.error(errMsg, 'Error');
        })
    }
    // Role onchange
    onChangeRole = (val) => {
    //    console.log("value1:",val)
        this.setState({ selectedRoleId: val});
     //   this.state.selectedRoleId = val ;
       // console.log("value2",this.state.selectedRoleId)
    }


    

    render() {  

        return (
            <div className="overflow-x-auto">
                {/* <NotificationContainer/> */}
                <form>
                    <h6 className="text-blue-500 text-sm mt-3 mb-6 font-light uppercase">
                        User Information
                    </h6>
                    <div className="flex flex-wrap">
                        <input type="file" 
                            ref={this.inputFileRef} 
                            onChange={(e)=>this.onChangeFileInput(e)} 
                            hidden
                        />
                        <div className="w-full lg:w-12/12 font-light">
                            <Image
                                src={this.state.fileSrc ? this.state.fileSrc : UploadImg}
                                rounded={true}
                                raised={false}
                                alt="Rounded Image"
                                style={{width: '120px', height: '120px', cursor: 'pointer'}}
                                onClick = {this.onClickOnImage}
                            />
                        </div>
                    </div>
                    <div className="flex flex-wrap mt-12">
                    
                        <div className="w-full lg:w-6/12 pr-4 font-light">
                            <Input
                                type="text"
                                color="blue"
                                placeholder="First Name"
                                value={this.state.first_name}
                                error = {this.state.errorFirstName}
                                onChange = {e => this.onChangeFirstName(e)}

                            />
                        </div>
                        <div className="w-full lg:w-6/12 pl-4 mb-12 font-light">
                            <Input
                                type="text"
                                color="blue"
                                placeholder="Last Name"
                                value={this.state.last_name}
                                error = {this.state.errorLastName}
                                onChange = {e => this.onChangeLastName(e)}
                                
                            />
                        </div>
                        
                        <div className="w-full lg:w-6/12 pr-4 font-light">
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label" className="font-light">Role</InputLabel>
                                <Select
                                    className="font-light"
                                    variant={"standard"}
                                    color="primary"
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    label="Role"
                                    value={this.state.selectedRoleId}
                                    onChange = {e => this.onChangeRole(e.target.value)}
                                >
                                    {this.state.rolesList.map((role,i) => (
                                     <MenuItem  key={i} value={role._id}>{role.title}</MenuItem>
                                  ))}
                                </Select>
                            </FormControl>
                        </div>
                        <div className="w-full lg:w-6/12 pl-4 mb-12 font-light">
                            <FormControl>
                                <FormLabel id="demo-row-radio-buttons-group-label">Gender</FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="demo-row-radio-buttons-group-label"
                                    name="row-radio-buttons-group"
                                    value={this.state.gender}
                                    onChange={(e) => this.setState({gender: e.target.value})}
                                >
                                    <FormControlLabel value="female" control={<Radio />} label="Female" />
                                    <FormControlLabel value="male" control={<Radio />} label="Male" />
                                </RadioGroup>
                            </FormControl>
                        </div>
                        <div className="w-full lg:w-6/12 pr-4 mb-12 font-light">
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                <MobileDatePicker
                                    label="Date Of Birth"
                                    value={this.state.dob}
                                    onChange={(newValue) => {
                                        this.setState({dob: newValue})
                                    }}
                                    renderInput={(params) => <TextField 
                                        variant={"standard"}
                                        color="primary"
                                        fullWidth {...params} 
                                    />}
                                />
                            </LocalizationProvider>
                        </div>
                        <div className="w-full lg:w-6/12 pl-4 mb-12 font-light">
                            <Input
                                type="text"
                                color="blue"
                                placeholder=""
                                placeholder="Enter Address (Optional)"
                                value={this.state.address}
                                onChange={(e) => this.setState({address: e.target.value})}
                            />
                        </div>
                        <div className="w-full lg:w-12/12 mb-12 font-light">
                            <Input
                                type="text"
                                color="blue"
                                placeholder=""
                                placeholder="Enter Place Of Birth (Optional)"
                                value={this.state.pob}
                                onChange={(e) => this.setState({pob: e.target.value})}
                            />
                        </div>
                        
                        <div className="w-full lg:w-12/12 mb-12 font-light">
                            <Textarea 
                            style={{height: '100px'}}
                            color="blue" 
                            placeholder="Enter Description (Optional)"
                            value={this.state.desc}
                            onChange={(e) => this.setState({desc: e.target.value})}
                            />
                        </div>  
                    </div>

                    
                    {/* Contact Info */}
                    <h6 className="text-blue-500 text-sm my-6 font-light uppercase">
                        Login Information
                    </h6>
                    
                    <div className="flex flex-wrap mt-10">
                        <div className="w-full lg:w-6/12 pr-4 mb-12 font-light">
                            <PhoneInput
                                enableSearch={true}
                                country={'kh'}
                                value={this.state.tel}
                                placeholder="Phone Number"
                                buttonStyle={{ 
                                    borderRadius: '0px',
                                    borderColor: 'white white lightGray white'
                                }}
                                inputStyle={{
                                    width: '100%',
                                    height: '41px',
                                    borderColor: 'white white lightGray white',
                                    borderRadius: '0px'
                                  }}
                                //onChange={tel => this.setState({tel})}
                                onChange={(value, country, e, formattedValue) => this.handleOnChangePhone(value, country, e, formattedValue)}
                            />
                        </div>
                        <div className="w-full lg:w-6/12 pl-4 mb-12 font-light">
                            <Input
                                type="email"
                                color="blue"
                                placeholder="Email Address"
                                value={this.state.email}
                                error = {this.state.errorEmail}
                                onChange = {e => this.onChangeEmail(e)}
                            />
                        </div>
                    
                        {/* Password */}
                        {!this.state.isEditMode ? 
                        <div className="w-full lg:w-6/12 pr-4 mb-12 font-light">
                            <Input
                                type="password"
                                color="blue"
                                placeholder="Password"
                                value={this.state.password}
                                error = {this.state.errorPassword}
                                onChange = {e => this.onChangePassword(e)}

                            />
                        </div>
                        :
                        null

                        }
                        
                        {/* Confirm Password */}
                        {!this.state.isEditMode ? 
                        <div className="w-full lg:w-6/12 pl-4 mb-12 font-light">
                            <Input
                                type="password"
                                color="blue"
                                placeholder="Enter confirm password" 
                                value={this.state.confirm_password}
                                error = {this.state.errorConfirmPassword}
                                onChange = {e => this.onChangeConfirmPassword(e)}
                            />
                        </div>
                        :
                        null

                        }
                    </div>



                    {/* Btn save */}
                    <div className="flex flex-wrap mt-10" style={{float:'right'}}>
                    
                    <Button color="red"
                        type = "button"
                        style={{marginRight:'10px'}}
                        onClick={()=>{this.props.callbackClickCancel()}}
                       
                    >
                        Cancel
                    </Button>

                    <Button color="lightBlue"
                        type = "button"
                        onClick={this.onClickBtnSave}
                    >
                        Save
                    </Button>
                </div>
                </form>
                
            </div>
        );
    }   // close render

}


export default CreateUser;



