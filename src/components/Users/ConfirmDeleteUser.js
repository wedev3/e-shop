import React, { useState } from "react";
import Modal from "@material-tailwind/react/Modal";
import ModalHeader from "@material-tailwind/react/ModalHeader";
import ModalBody from "@material-tailwind/react/ModalBody";
import ModalFooter from "@material-tailwind/react/ModalFooter";
import Button from "@material-tailwind/react/Button";

const ConfirmDeleteUser = (props) => {
    const [showModal, setShowModal] = React.useState(props.isShowModal);
    const user = props.deleteData;
    var full_name = "";
    if(user){
        full_name += user.first_name + " ";
    }
    if(user){
        full_name += user.last_name;
    }
   
    const cancelAction = () => {
        props.onClickCancelDelete(user)
    }
    const okAction = () => {
        props.onClickOkDelete(user)
    }

    
    
    return (
        <>
            <Modal size="sm" active={showModal} toggler={() => setShowModal(false)}> 
                <ModalHeader toggler={() => setShowModal(false)}>
                    Confirmation
                </ModalHeader>
                <ModalBody>
                    <p className="text-base leading-relaxed text-gray-600 font-normal">
                        Are you sure to delete {user ? full_name : ''} ?
                    </p>
                </ModalBody>
                <ModalFooter>
                    <Button 
                        color="red"
                        buttonType="link"
                        onClick={()=>cancelAction()}
                        ripple="dark"
                    >
                        Cancel
                    </Button>

                    <Button
                        color="green"
                        buttonType="link"
                        onClick={()=>okAction()}
                        ripple="light"
                    >
                        Yes
                    </Button>
                </ModalFooter>
            </Modal>
        </>
    );
}

export default ConfirmDeleteUser