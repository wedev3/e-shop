
import React from "react";
import Card from '@material-tailwind/react/Card';
import CardHeader from '@material-tailwind/react/CardHeader';
import CardBody from '@material-tailwind/react/CardBody';
import  CardFooter from '@material-tailwind/react/CardFooter';
import Image from '@material-tailwind/react/Image';
import Button from "@material-tailwind/react/Button"; 
import Pagination from "@material-tailwind/react/Pagination";
import PaginationItem from "@material-tailwind/react/PaginationItem";
import Icon from "@material-tailwind/react/Icon";
import CreateUser from "./createUser";
import {FaArrowLeft, FaPlus} from 'react-icons/fa'
import UserProfile from 'assets/img/defaulf_photo.png';
import 'bootstrap';
import ConfirmDeleteUser from './ConfirmDeleteUser';

import { limit } from 'controllers/responseHelper';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const helper = require('../helper').default;
const axios = require('axios').default

const List = Symbol('LIST_MENU');
const Create = Symbol('CREATE_MENU'); 
const Edit = Symbol('EDIT_MENU');

const PRE_PAGE = Symbol('PRE_PAGE');
const NUM_PAGE = Symbol('NUM_PAGE');
const NEXT_PAGE = Symbol('NEXT_PAGE');


 class UserTableCard extends React.Component {

    //constructor
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            mode: List,
            headerTitle: 'User',
            selectedUser: null,
            isLoaded: false,
            isShowModal: false,

            currentPage: 1,
            page: 1,
            limit: 6,
            totalPage: 0
          
        };
    }
    componentDidMount() {
        this.fetchUserList(this.state.page)
    }

    // list user 
    fetchUserList = (page) => {
        axios({
            method: 'GET',
            url: helper.baseUrl+'/users',
            headers: helper.headers,
            params: {page: page, limit: this.state.limit}
        }).then(res => {
            console.log('success res = ', res.data.data);
            // this.setState({items: res.data.data});

            this.setState({items: res.data.data});
            let totalCount = res.data.pagination.totalCount
            var _totalPage = totalCount/this.state.limit 
            _totalPage = parseInt(_totalPage)
            if(totalCount%this.state.limit>0){
                _totalPage += 1
            }
            this.setState({totalPage: _totalPage});
            console.log('totalpage = ', this.state.totalPage)

 
        }).catch(err => {
            console.log("error:",err.response.data.message);
            let errMsg = err.response.data.message
            NotificationManager.error(errMsg, 'Error');
        })
    }
    // click add 
    onClikAddUser = () => {
        if (this.state.mode == List) {
            this.setState({mode: Create});
            this.setState({headerTitle: 'Create User'});
        } 
        if (this.state.mode == Create) {
            this.setState({mode: List});
            this.setState({headerTitle: 'Users'});
        }
        if (this.state.mode == Edit) {
            this.setState({mode: List});
            this.setState({headerTitle: 'Users'});
        }
    }

    // Edit User
    onClickEditUser = (user) => {
        this.setState({mode: Edit});
        this.setState({headerTitle: 'Edit User'});
        this.setState({isShowModal: false});
        this.setState({selectedUser: user});
    }


    // Click SHow Modal Delete
    onClickDeleteUser = (user) => {
        this.setState({selectedUser: user});
        this.setState({isShowModal: true});
    }

    // Cancel Delete
    onClickCancelDelete = (user) => {
        this.setState({isShowModal: false});
    }

    onClickOkDelete = (user) => {
        axios({
            method: 'DELETE',
            url: helper.baseUrl+'/user/'+user._id,
            headers: helper.headers
        }).then(res => {
            console.log('res delete user = ', res.data)
            let index = this.state.items.indexOf(user)
            this.state.items.splice(index,1)
            this.setState({isShowModal: false});
            NotificationManager.success('Successfully delete user.', 'Success');
        }).catch(err => {
            console.log("error:",err.response)
        })
    }


    // SUCESS
    didSaveUserSuccess = (isEditMode) => {
        NotificationManager.success(isEditMode ? 'Successfully updated user.' : 'Successfully saved user.', 'Success');
        this.fetchUserList();
        this.setState({mode: List});
    }

    didClickCancelForm = () => {
        this.setState({mode: List});
    }
    
    onClickPageItem = (page, pageType) => {
        switch (pageType) {
            case PRE_PAGE:
                
                if(this.state.currentPage > 1) {
                    let _page = this.state.currentPage -= 1;
                    this.setState({currentPage: _page});
                    this.fetchUserList(this.state.currentPage);
                   
                } else {
                    NotificationManager.info('No page available', 'Info');
                }
            case NUM_PAGE:
                this.setState({currentPage: page})
                this.fetchUserList(page);
                
            case NEXT_PAGE:
                if(this.state.currentPage < this.state.totalPage) {
                    let _page = this.state.currentPage += 1;
                    this.setState({currentPage: _page});
                    this.fetchUserList(_page);
                } else {
                    NotificationManager.info('End of page.', 'Info');
                }
        }
        
    }
    
    // render data
     render() {

        const pageItemMouseOverStyle = {
            cursor: 'pointer'
        }

        // Modal Delete
        const MODAL = () => {
            if (this.state.isShowModal) {
                return (<ConfirmDeleteUser 
                    isShowModal={true}
                    onClickCancelDelete={this.onClickCancelDelete}
                    onClickOkDelete={this.onClickOkDelete}
                    deleteData={this.state.selectedUser}
                />)
            }else {
               
                return (<ConfirmDeleteUser 
                    isShowModal={false}
                    onClickCancelDelete={this.onClickCancelDelete}
                    onClickOkDelete={this.onClickOkDelete}
                    deleteData={this.state.selectedUser}
                />)
            }
        }


        // Switch Icon Header
        const HEADERICON = () => {
            return this.state.mode == List ? (<FaPlus/>) : (<FaArrowLeft/>)
        }
        
        const BODY = () => {
            switch(this.state.mode) {
                    case List: 
                    return ( 
                        <div className="overflow-x-auto">

                            <table className="items-center w-full bg-transparent border-collapse">
                                <thead>
                                    <tr>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            First Name
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Last Name
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Status
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Role
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Profile
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                {this.state.items.map((item,i) => (
                                
                                    <tr key={i}> 
                                    <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                        {item.first_name}
                                    </th>
                                    <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                        {item.last_name}
                                    </th>
                                    <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                        <i className="fas fa-circle fa-sm text-orange-500 mr-2"></i>{' '}
                                        pending
                                    </th>
                                    <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                        <i className="fas fa-circle fa-sm text-orange-500 mr-2"></i>{' '}
                                        Admin
                                    </th>
                                    <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                        <div className="flex">
                                            <div className="w-10 h-10 rounded-full border-2 border-white">
                                                <Image
                                                    style={{height: "50px"}}
                                                    src={item.profile ? item.profile : UserProfile }
                                                    rounded
                                                    alt=""
                                                />
                                            </div>
                                            
                                        </div>
                                    </th>
                                    
                                    <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                            <div className="flex">
                                                <div className="flex-auto">                                          
                                                {/* show modal detail */}
                                                <Button color="cyan" ripple="light"
                                                       // onClick={()=>this.onClickEditUser(item)}
                                                    >
                                                        <Icon name="visibility" />
                                                    </Button>
                                                </div>
                                                <div className="flex-auto">
                                                
                                                        <Button color="teal" ripple="light" 
                                                        onClick={()=>this.onClickEditUser(item)}
                                                        >
                                                            <Icon name="edit" />
                                                        </Button>
                                                
                                                </div>
                                                <div className="flex-auto">
                                                    <Button color="red" ripple="light"
                                                        onClick={()=>this.onClickDeleteUser(item)}>
                                                        <Icon name="delete" />
                                                    </Button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                ))}
                    
                                </tbody>
                            </table>
                            <_Pagination/>
                        </div>
        
                    );
    
                    // case Create: return (<CreateUser isEditMode={false}/>)
                    // case Edit: return (<CreateUser isEditMode={true} editData={this.state.selectedUser}/>)

                    case Create: return (<CreateUser isEditMode={false} callbackUserSaveSuccess={()=>this.didSaveUserSuccess(false)} callbackClickCancel={()=>this.didClickCancelForm()}/>)
                    case Edit: return (<CreateUser isEditMode={true} editData={this.state.selectedUser} callbackUserSaveSuccess={()=>this.didSaveUserSuccess(true)} callbackClickCancel={()=>this.didClickCancelForm()}/>)
                }
            }

            // PAGINATIO 
            const _Pagination = () => {
                var pageItems = []
                pageItems.push( 
                    <PaginationItem ripple="dark" onClick = {()=>this.onClickPageItem(null,PRE_PAGE)} style={pageItemMouseOverStyle}>
                        <Icon name="keyboard_arrow_left" />
                    </PaginationItem>
                )
                for (let i = 0; i<this.state.totalPage; i++) {
                    let _page = parseInt(i+1)
                    pageItems.push( 
                        <PaginationItem 
                            color={this.state.currentPage === _page ? 'lightBlue' : ''} 
                            onClick = {() => this.onClickPageItem(_page, NUM_PAGE)}
                            style={pageItemMouseOverStyle}
                            ripple="dark">
                            {_page}
                        </PaginationItem>
                    )
                }
                pageItems.push( 
                    <PaginationItem ripple="dark" onClick = {()=>this.onClickPageItem(null, NEXT_PAGE)}
                        style={pageItemMouseOverStyle}
                    >
                        <Icon name="keyboard_arrow_right" />
                    </PaginationItem>
                )
                return (
                    <Pagination>
                    {pageItems.map((item) =>
                        <div>{item}</div>
                    )}
                    </Pagination>
                )
            }

            return (
                <div>
                    <NotificationContainer/>
                <Card>
                    <CardHeader color="blue" contentPosition="none">
                        <div className="w-full flex items-center justify-between">
                            <h2 className="text-white text-2xl">{this.state.headerTitle}</h2>
                            <Button
                                    color="lightBlue"
                                    buttonType="filled"
                                    size="regular"
                                    rounded={false}
                                    block={false}
                                    iconOnly={false}
                                    ripple="light"
                                    className="rounded-full h-16 w-16 align-top bg-light-blue-500"
                                    onClick={this.onClikAddUser}
                                >
                                    <HEADERICON/>
                            </Button>
                        </div>
                    </CardHeader>
                    
                    <CardBody>
                        <BODY/>
                    </CardBody>
                  
                    <MODAL/>
                </Card>
                </div>
            );

        }   // close render
    
    }


export default UserTableCard;

