import React, { useState } from "react";
import Modal from "@material-tailwind/react/Modal";
import ModalHeader from "@material-tailwind/react/ModalHeader";
import ModalBody from "@material-tailwind/react/ModalBody";
import ModalFooter from "@material-tailwind/react/ModalFooter";
import Button from "@material-tailwind/react/Button";
import Icon from "@material-tailwind/react/Icon";

const helper = require('../helper').default;
const axios = require('axios').default

export default function ShowUserDetail(data) { 
   
    const [showModal,setShowModal] = React.useState(data.isShowModal);

    //constructor
    // constructor(props) {
    //     super(props);
    //     this.state={
    //         id:this.props.id,
    //         first_name:this.props.first_name,
    //         last_name:this.props.last_name,
    //         email:this.props.email,
    //         gender:this.props.gender,
    //         tel:this.props.tel,
    //         pob:this.props.pob,
    //         dob:this.props.dob,
    //         address:this.props.address,
    //         desc:this.props.desc
    //     };
    // }

    // fetchUserDetails=(id)=>{
    //     axios({
    //         method: 'GET',
    //         url: helper.baseUrl+'/user'+id,
    //         headers: {
    //             "Access-Control-Allow-Origin": "*",
    //             "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
    //             "Access-Control-Allow-Headers": "Authorization",
    //             "Content-Type": "application/json",
    //             "Authorization" : "Bearer "+token
    //         },
    //     }).then(res => {
    //         console.log('success res = ', res.data.data);
    //         //this.setState({items: res.data.data});

    //         this.setState({first_name:res.data.results[0].first_name});
    //         this.setState({last_name:res.data.results[0].last_name});
    //         this.setState({email:res.data.results[0].email});
    //         this.setState({tel:res.data.results[0].tel});
    //         this.setState({pob:res.data.results[0].pob});
    //         this.setState({dob:res.data.results[0].dob});
    //         this.setState({address:res.data.results[0].address});
    //         this.setState({desc:res.data.results[0].desc});


    //     }).catch(err => {
    //         console.log("error:",err)
    //     })
    // };

    // componentDidMount(){
    //     this.fetchUserDetails(this.state.id);
    // };


    return (
        <>
           {/* show button detail user */}
            {/* <Button color="cyan" ripple="light" onClick={(e) => setShowModal(true)}>
                <Icon name="visibility" />
            </Button> */}

            <Modal size="regular" active={showModal} toggler={() => setShowModal(false)}>
                <ModalHeader toggler={() => setShowModal(false)}>
                    Modal Title
                </ModalHeader>
                <ModalBody>
                    <p className="text-base leading-relaxed text-gray-600 font-normal">
                        I always felt like I could do anything. That’s the main thing people
                        I always felt like I could do anything. That’s the main thing people
                    </p>
                    
                </ModalBody>
                <ModalFooter>
                    <Button 
                        color="red"
                        buttonType="link"
                        onClick={(e) => setShowModal(false)}
                        ripple="dark"
                    >
                        Close
                    </Button>

                    <Button
                        color="green"
                        onClick={(e) => setShowModal(false)}
                        ripple="light"
                    >
                        Save Changes
                    </Button>
                </ModalFooter>
            </Modal>
        </>
    );
}
 