import Card from '@material-tailwind/react/Card';
import CardHeader from '@material-tailwind/react/CardHeader';
import CardBody from '@material-tailwind/react/CardBody';
import Image from '@material-tailwind/react/Image';
import Progress from '@material-tailwind/react/Progress';
import Team1 from 'assets/img/team-1-800x800.jpg';
import Button from "@material-tailwind/react/Button";
import Icon from '@material-tailwind/react/Icon';
import React from 'react';
import CreateProduct from './CreateProduct';
import {FaArrowLeft, FaPlus} from 'react-icons/fa'
import Pagination from "@material-tailwind/react/Pagination";
import PaginationItem from "@material-tailwind/react/PaginationItem";

import ConfirmDeleteProduct from './ConfirmDeleteProduct';

import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { limit } from 'controllers/responseHelper';
import { sys } from 'typescript'; 
 

 
const helper = require('../helper').default;
const axios = require('axios').default

const List = Symbol('LIST_MENU');
const Create = Symbol('CREATE_MENU');
const Edit = Symbol('EDIT_MENU');

const PRE_PAGE = Symbol('PRE_PAGE');
const NUM_PAGE = Symbol('NUM_PAGE');
const NEXT_PAGE = Symbol('NEXT_PAGE');

const baseIconPath = '/src/assets/uploads/'

export default class MenuList extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {
            products: [],
            mode: List,
            headerTitle: 'Products',
            selectedMenu: null,
            isShowModal: false,
            
            currentPage: 1,
            page: 1,
            limit: 2,
           // totalPage: 0
        }
    }

    componentDidMount() {
        this.fetchMenuList(this.state.page) 
    }

    fetchMenuList = (page) => {
       // console.log('page  = ', page);
        axios({
            method: 'GET',
            url: helper.baseUrl+'/products',
            headers: helper.headers,
           // params: {page: page, limit: this.state.limit}
        }).then(res => {
            console.log("Products Data:",res.data.data);

             this.setState({products: res.data.data});
            // let totalCount = res.data.pagination.totalCount
            // var _totalPage = totalCount/this.state.limit 
            // _totalPage = parseInt(_totalPage)
            // if(totalCount%this.state.limit>0){
            //     _totalPage += 1
            // }
            // this.setState({totalPage: _totalPage});
            // console.log('totalpage = ', this.state.totalPage)
            
        }).catch(err => {
            // console.log("error:",err.response.data.message);
            // let errMsg = err.response.data.message
            // NotificationManager.error(errMsg, 'Error');
        })
    }

    onClikAddProduct = () => {
        if (this.state.mode == List) {
            this.setState({mode: Create});
            this.setState({headerTitle: 'Create Product'});
        } 
        if (this.state.mode == Create) {
            this.setState({mode: List});
            this.setState({headerTitle: 'Products'});
        }
        if (this.state.mode == Edit) {
            this.setState({mode: List});
            this.setState({headerTitle: 'Products'});
        }
    }

    onClickEditMenu = (menu) => {
        this.setState({mode: Edit});
        this.setState({headerTitle: 'Edit Products'});
        this.setState({isShowModal: false});
        this.setState({selectedMenu: menu});
    }

    onClickCancel = (menu) => {
        this.setState({isShowModal: false});
    }
    onClickOk = (menu) => {
        axios({
            method: 'DELETE',
            url: helper.baseUrl+'/product/'+menu._id,
            headers: helper.headers
        }).then(res => {
            console.log('res delete product = ', res.data)
            let index = this.state.products.indexOf(menu)
            this.state.products.splice(index,1)
            this.setState({isShowModal: false});
            NotificationManager.success('Successfully delete product.', 'Success');
        }).catch(err => {
            console.log("error:",err.response)
        })
    }

    onClickDeleteProduct = (menu) => {
        this.setState({selectedMenu: menu});
        this.setState({isShowModal: true});
    }

    didSaveMenuSuccess = () => {
        NotificationManager.success('Successfully saved products.', 'Success');
        this.fetchMenuList();
        this.setState({mode: List});
    }

    // onClickPageItem = (page, pageType) => {
    //     switch (pageType) {
    //         case PRE_PAGE:
                
    //             if(this.state.currentPage > 1) {
    //                 this.state.currentPage -= 1;
    //                 this.fetchMenuList(this.state.currentPage);
    //                 return
    //             } else {
    //                 return
    //             }
    //         case NUM_PAGE:
    //             this.setState({currentPage: page})
    //             this.fetchMenuList(this.state.currentPage);
    //         case NEXT_PAGE:
    //             if(this.state.currentPage < this.state.totalPage) {
    //                 this.state.currentPage += 1;
    //                 this.fetchMenuList(this.state.currentPage);
    //                 return
    //             } else {
    //                 return
    //             }
    //     }
        
    // }
    
    
    render() {
        const pageItemMouseOverStyle = {
            cursor: 'pointer'
        }
        const MODAL = () => {
            if (this.state.isShowModal) {
                return (<ConfirmDeleteProduct 
                    isShowModal={true}
                    onClickCancel={this.onClickCancel}
                    onClickOk={this.onClickOk}
                    deleteData={this.state.selectedMenu}
                />)
            } else {
                return (<ConfirmDeleteProduct 
                    isShowModal={false}
                    onClickCancel={this.onClickCancel}
                    onClickOk={this.onClickOk}
                    deleteData={this.state.selectedMenu}
                />)
            }
        }

        const HEADERICON = () => {
            return this.state.mode == List ? (<FaPlus/>) : (<FaArrowLeft/>)
        }

        const BODY = () => {
            switch(this.state.mode) {
                case List: 
                    return (
                        <div className="overflow-x-auto">
                            <table className="items-center w-full bg-transparent border-collapse">
                                <thead>
                                    <tr>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Code
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Name
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Price
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Quantity
                                        </th>
                                        <th className="px-2 text-blue-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {this.state.products.map((product, i) => (
                                     //  console.log("Data Products:",product)
                                    
                                        <tr key={i}> 
                                          
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                {product.code}
                                            </th>
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                {product.name}
                                            </th>
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                {product.price}
                                            </th>
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                {product.qty}
                                            </th>
                                           
                                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                                                <div className="flex">
                                                <div className="flex-auto">
                                                        <Button color="cyan" ripple="light">
                                                            <Icon name="visibility" />
                                                        </Button>
                                                    </div>
                                                    <div className="flex-auto">
                                                        <Button color="teal" ripple="light"
                                                            onClick={()=>this.onClickEditMenu(product)}
                                                        >
                                                            <Icon name="edit" />
                                                            
                                                        </Button>
                                                    </div>
                                                    <div className="flex-auto">
                                                        <Button color="red" ripple="light"
                                                            onClick={()=>this.onClickDeleteProduct(product)}
                                                        >
                                                            <Icon name="delete" />
                                                        </Button>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            {/* <_Pagination/> */}
                        </div>
                    )
                    case Create: return (<CreateProduct isEditMode={false} callbackMenuSaveSuccess={()=>this.didSaveMenuSuccess()}/>)
                    case Edit: return (<CreateProduct isEditMode={true} editData={this.state.selectedMenu} callbackMenuSaveSuccess={()=>this.didSaveMenuSuccess()}/>)
            }
        }

        // const _Pagination = () => {
        //     var pageItems = []
        //     pageItems.push( 
        //         <PaginationItem ripple="dark" onClick = {()=>this.onClickPageItem(null,PRE_PAGE)} style={pageItemMouseOverStyle}>
        //             <Icon name="keyboard_arrow_left" />
        //         </PaginationItem>
        //     )
        //     for (let i = 0; i<this.state.totalPage; i++) {
        //         let _page = parseInt(i+1)
        //         pageItems.push( 
        //             <PaginationItem 
        //                 color={this.state.currentPage === _page ? 'lightBlue' : ''} 
        //                 onClick = {() => this.onClickPageItem(_page, NUM_PAGE)}
        //                 style={pageItemMouseOverStyle}
        //                 ripple="dark">
        //                 {_page}
        //             </PaginationItem>
        //         )
        //     }
        //     pageItems.push( 
        //         <PaginationItem ripple="dark" onClick = {()=>this.onClickPageItem(null, NEXT_PAGE)}
        //             style={pageItemMouseOverStyle}
        //         >
        //             <Icon name="keyboard_arrow_right" />
        //         </PaginationItem>
        //     )
        //     return (
        //         <Pagination>
        //            {pageItems.map((item) =>
        //                 <div>{item}</div>
        //            )}
        //         </Pagination>
        //     )
        // }
        return (
            <div>
                <NotificationContainer/>
            <Card>
                <CardHeader color="blue" contentPosition="none">
                    <div className="w-full flex items-center justify-between">
                        <h2 className="text-white text-2xl">{this.state.headerTitle}</h2>
                        <Button
                                color="lightBlue"
                                buttonType="filled"
                                size="regular"
                                rounded={false}
                                block={false}
                                iconOnly={false}
                                ripple="light"
                                className="rounded-full h-16 w-16 align-top bg-light-blue-500"
                                 onClick={this.onClikAddProduct}
                            >
                                <HEADERICON/>
                        </Button>
                    </div>
                </CardHeader>
                <CardBody>
                    <BODY/>
                </CardBody>
                <MODAL/>
            </Card>
            </div>
        );
    }
}
