import Card from '@material-tailwind/react/Card';
import CardHeader from '@material-tailwind/react/CardHeader';
import CardBody from '@material-tailwind/react/CardBody';
import Button from '@material-tailwind/react/Button';
import Input from '@material-tailwind/react/Input';
import Image from "@material-tailwind/react/Image";
import UploadImg from 'assets/img/upload.png';
import FormControl from '@mui/material/FormControl';
//import Select from 'react-select'
import Select, { SelectChangeEvent } from '@mui/material/Select';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';

import React from 'react';
import axios from 'axios'; 
import helper from '../helper'; 

// Product Category
const product_category = [
    { value: 'Drink', label: 'Cocacola' },
    { value: 'Cloth', label: 'T-shirt' },
    { value: 'Fruit', label: 'Banana' }
];

const product_brand = [
    { value: 'LV', label: 'lv' },
    { value: 'LACOSE', label: 'locose' },
    { value: 'POLO', label: 'polo' }
];

const product_supplier = [
    { value: 'ALIBABA', label: 'ALIBABA' },
    { value: 'AMAZON', label: 'AMAZON' },
    { value: 'LAZADA', label: 'LAZADA' }
];



export default class CreateProduct extends React.Component {
    constructor(props) {
        super(props) 
        this.inputFileRef = React.createRef(); 
        this.state = {
            isEditMode: this.props.isEditMode,
            editData: this.props.editData,
            
        //    fileSrc: null,
            errorCode: null,
            errorName: null,
           
            code: null,
            name: null,
            price: null,
            qty: null,
            countInStock: null,
            productCategoryList:[],
            productBrandList:[],
            productSupplierList:[],
            selcetProductCateId: null,
            selectProductBrandId:null,
            selectProductSupplierId:null,


        }
    }

    

    componentDidMount() {

        this.fetchProductCategoryList();
        this.fetchProductBrandList();
        this.fetchProductSupplierList();

        var random = Math.floor(Math.random() * 3) + 1;
        this.setState({code: "C_" +random});

        if (this.state.isEditMode) {

            this.setState({code: this.state.editData.code});
            this.setState({name: this.state.editData.name});
            this.setState({qty: this.state.editData.qty});
            this.setState({price: this.state.editData.price});
            this.setState({countInStock: this.state.editData.countInStock});
            // if (this.state.editData.icon) {
            //     this.setState({fileSrc: this.state.editData.icon})
            // }
        }
    }

    callbackMenuSaveSuccess = () => {
        this.props.callbackMenuSaveSuccess()
    }

    onClickBtnSave = () => {
        if (this.state.code == null || this.state.code == "") {
            this.setState({errorCode: "Code is required."});
        } else if (this.state.name == null || this.state.name == "") {
            this.setState({errorName: "Name is required."});
        } else {
            var body = {
                "code": this.state.code,
                "name": this.state.name,
                "price": this.state.price,
                "qty": this.state.qty,
                "countInStock": this.state.countInStock,
                "status" : this.state.status

            }
            
            this.requestSave(body)  
        } 
    } 

    requestSave = (body) => {
        var method = this.state.isEditMode ? "PATCH" : "POST"
        var suffix = this.state.isEditMode ? '/product/'+this.state.editData._id : '/product'
        axios({
            
            method: method,
            url: helper.baseUrl+suffix,
            headers: helper.headers,
            data: body
        }).then(res => {
            console.log('Create Products data = ', res.data)
            this.callbackMenuSaveSuccess();
        }).catch(err => {
            console.log("error:",err)
        })
    }

    onChangeCode = (e) => {
        this.setState({ code: e.target.value })
        this.setState({errorCode: this.state.code ? null : 'Code is required.'});
    }

    onChangeName= (e) => {
        this.setState({ name: e.target.value });
        this.setState({errorName: this.state.name ? null : 'Name is required.'});
    }


    //1.  Product Category 
    fetchProductCategoryList = () => {
        axios({
            method: 'GET',
            url: helper.baseUrl+'/productCategorys',
            headers: helper.headers,
           
        }).then(res => {
            this.setState({productCategoryList: res.data.data});
           
        }).catch(err => {
            console.log("error:",err.response.data.message);
            let errMsg = err.response.data.message
           // NotificationManager.error(errMsg, 'Error');
        })
    }
    // Product category onchange
    onChangeProductCategory = (val) => {
        
        this.setState({ selectedRoleId: val});
    }

    //2.  Product Brand 
    fetchProductBrandList = () => {
        axios({
            method: 'GET',
            url: helper.baseUrl+'/productBrands',
            headers: helper.headers,
           
        }).then(res => {
            this.setState({productBrandList: res.data.data});
           
        }).catch(err => {
            console.log("error:",err.response.data.message);
            let errMsg = err.response.data.message
           // NotificationManager.error(errMsg, 'Error');
        })
    }
    // Product brand onchange
    onChangeProductBrand = (val) => {
        
        this.setState({ selectedRoleId: val});
    }



    //3.  Product Supplier 
    fetchProductSupplierList = () => {
        axios({
            method: 'GET',
            url: helper.baseUrl+'/productSuppliers',
            headers: helper.headers,
           
        }).then(res => {
            this.setState({productSupplierList: res.data.data});
            console.log("suppliers:",res.data.data)
             
        }).catch(err => {
            console.log("error:",err.response.data.message);
            let errMsg = err.response.data.message
           // NotificationManager.error(errMsg, 'Error');
        })
    }
    // Product Brand onchange
    onChangeProductSupplier = (val) => {

        this.setState({ selectedRoleId: val});
    }
    




    

    render() {
        
        return (
            <div className="overflow-x-auto">
                <form>
                    <h6 className="text-blue-500 text-sm mt-3 mb-6 font-light uppercase">
                        Enter Product Information
                    </h6>
                    <div className="flex flex-wrap mt-10">

                        <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
                            <Input
                                type="text"
                                color="blue"
                                placeholder="Code"
                                style={{opacity:'0.5'}}
                                disabled={true}
                                value={this.state.code}
                                error={this.state.errorCode}
                                onChange = {e => this.onChangeCode(e)}
                            />
                        </div>
                        <div className="w-full lg:w-6/12 pl-4 mb-10 font-light">
                            <Input
                                type="text"
                                color="blue"
                                placeholder="Name"
                                value={this.state.name}
                                error={this.state.errorName}
                                onChange = {e => this.onChangeName(e)}
                            />
                        </div>
                        {/* Product Category/Brand/Supplier */}
                        
                        <div className="w-full lg:w-4/12 pr-4 font-light"> 
                        <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label" className="font-light">Product Category</InputLabel>
                                <Select
                                    className="font-light"
                                    variant={"standard"}
                                    color="secondary"
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    label="Product Category"
                                    value={this.state.selectedRoleId}
                                    onChange = {e => this.onChangeProductCategory(e.target.value)}
                                >
                                    {this.state.productCategoryList.map((category,i) => (
                                     <MenuItem  key={i} value={category._id}>{category.name}</MenuItem>
                                  ))}
                                </Select>
                            </FormControl>
                        </div>

                        <div className="w-full lg:w-4/12 pr-4 font-light"> 
                        <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label" className="font-light">Product Brand</InputLabel>
                                <Select
                                    className="font-light"
                                    variant={"standard"}
                                    color="secondary"
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    label="Product Brand"
                                    value={this.state.selectedRoleId}
                                    onChange = {e => this.onChangeProductBrand(e.target.value)}
                                >
                                    {this.state.productBrandList.map((brand,i) => (
                                     <MenuItem  key={i} value={brand._id}>{brand.name}</MenuItem>
                                  ))}
                                </Select>
                            </FormControl>
                        </div>


                        <div className="w-full lg:w-4/12 pr-4 font-light"> 
                        <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label" className="font-light">Pruduct Supplier</InputLabel>
                                <Select
                                    className="font-light"
                                    variant={"standard"}
                                    color="secondary"
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    label="Product Supplier"
                                    value={this.state.selectedRoleId}
                                    onChange = {e => this.onChangeProductSupplier(e.target.value)}
                                >
                                    {this.state.productSupplierList.map((supplier,i) => (
                                     <MenuItem  key={i} value={supplier._id}>{supplier.name}</MenuItem>
                                  ))}
                                </Select>
                            </FormControl>
                        </div> 
                   

                        <div className="w-full lg:w-12/12 mb-10 font-light">
                            <Input
                                type="number"
                                color="blue"
                                placeholder="Price"
                                value={this.state.price}
                                onChange = {e => this.setState({ price: e.target.value })}
                            />
                        </div>

                        <div className="w-full lg:w-12/12 mb-10 font-light">
                            <Input
                                type="number"
                                color="blue"
                                placeholder="Quantity"
                                value={this.state.qty}
                            onChange = {e => this.setState({ qty: e.target.value })}
                            />
                        </div>

                        <div className="w-full lg:w-12/12 mb-10 font-light">
                            <Input
                                type="number"
                                color="blue"
                                placeholder="Count In Stock"
                                value={this.state.countInStock}
                                onChange = {e => this.setState({ countInStock: e.target.value })}
                            />
                        </div>

                        

                    </div>

                    
                    <div className="flex flex-wrap mt-10" style={{float:'right'}}>
                        <Button color="red"
                                type = "button"
                                style={{marginRight:'10px'}}
                                //onClick={this.onClickBtnSave}
                            >
                            Cancel
                        </Button>
                        <Button color="lightBlue"
                            type = "button"
                            onClick={this.onClickBtnSave}
                        >
                            Save
                        </Button>
                    </div>
                </form>
            </div>
        )
    }
}
