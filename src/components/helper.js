
const savedUser = JSON.parse(localStorage.getItem('SAVED_USER'));
var token = savedUser ? savedUser.token : ""
const helper = {
    baseUrl: 'http://localhost:5000',
    headers: {
        "Content-Type": "application/json",
        "Authorization" : "Bearer "+ token
        
    },
    headersFormData: {
        "Content-Type": "multipart/form-data",
        "Authorization" : "Bearer "+ token
    },
    savedUser: savedUser,
    isLoggedIn: localStorage.getItem('IS_LOGGED_IN') || false
}
export default helper;