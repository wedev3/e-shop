
import { useEffect } from 'react';
import { Navigate } from 'react-router';
import {Route} from 'react-router-dom'

const PrivateRoute = ({ component: Component, isAuth, path, ...props }) => {
    useEffect(()=> {
        //Component
    });
    if(!isAuth) {
        return <Navigate to="/login" />;
    }
    return <Route exact path={path} element={<Component />} />
}
export default PrivateRoute;

