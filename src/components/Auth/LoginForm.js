import React from "react";
import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import CardFooter from "@material-tailwind/react/CardFooter";
import InputIcon from "@material-tailwind/react/InputIcon";
import Button from "@material-tailwind/react/Button";
import H5 from "@material-tailwind/react/Heading5";
import Alert from '@material-tailwind/react/Alert';
import { useNavigate, Navigate, NavLink } from 'react-router-dom';
import {NotificationContainer, NotificationManager} from 'react-notifications';
// import Users from '../../pages/Users';


const baseUrl = require('../helper').default.baseUrl;
const axios = require('axios').default

const mystyle = {
    margin: "auto",
    width: "60%",
    padding: "10px"
};

export default class LoginForm extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {
            email: '',
            password: '',
            errorEmail: null,
            errorPass: null,
            errorResponse: 'Error Msg',
            hideAlert: true,
        }
    }

    clickLoginAction = () => {  
        // this.setState({loginSuceeded: true});
        // return 
        if (this.state.email === '') {
            this.setState({errorEmail: 'Email Address is required'})
        } else {
            this.setState({errorEmail: null})
        }
        if (this.state.password === '') {
            this.setState({errorPass: 'Password is required'})
        } else {
            this.setState({errorPass: null})
        }

        if (this.state.email !== '' && this.state.password !== '') {
            var params = {
                email: this.state.email,
                password: this.state.password
            }
            axios({
                method: 'POST',
                url: baseUrl+'/login',
                data: params
            })
            .then(res => {
                localStorage.setItem('SAVED_USER', JSON.stringify(res.data.data));
                localStorage.setItem('IS_LOGGED_IN', true);
                window.location.reload(false);
            })
            .catch(err => {
                let errorMsg = err.response.data.message;
                NotificationManager.error(errorMsg, 'Error');
                /*this.setState({errorResponse: errorMsg});
                this.setState({hideAlert: false});
                setTimeout(function() {
                    this.setState({hideAlert: true});
                }.bind(this), 2000);*/
            });
            
        }
    }

    
    render() {
        return (
            
            <div style={mystyle}>
                <NotificationContainer/>
                <div style={{width:'80%'}} className="mx-auto">
                    <Alert color="red" style={{display: this.state.hideAlert ? 'none': 'block'}}>{this.state.errorResponse}</Alert>
                </div>
                <div style={{marginTop:'50px'}}>
                    <Card className="w-96">
                        <CardHeader color="lightBlue" size="lg">
                            <H5 color="white">Login</H5>
                        </CardHeader>
            
                        <CardBody>
                            <div className="mb-8 px-4">
                                <InputIcon
                                    type="email"
                                    color="lightBlue"
                                    placeholder="Email Address"
                                    iconName="email"
                                    name = "email"
                                    error = {this.state.errorEmail}
                                    value = {this.state.email}
                                    onChange = {e => this.setState({ email: e.target.value })}
                                />
                            </div>
                            <div className="mb-4 px-4">
                                <InputIcon
                                    type="password"
                                    color="lightBlue"
                                    placeholder="password"
                                    iconName="lock"
                                    name = "password"
                                    error = {this.state.errorPass}
                                    value = {this.state.passowrd}
                                    onChange = {e => this.setState({ password: e.target.value })}
                                />
                            </div>
                        </CardBody>
                        <CardFooter>
                            <div className="flex justify-center">
                                <>
                                <Button
                                    color="lightBlue"
                                    buttonType="link"
                                    size="lg"
                                    ripple="dark"
                                    onClick = {this.clickLoginAction.bind(this)}
                                >
                                    Login
                                </Button>
                                </>
                            </div>
                        </CardFooter>
                    </Card>
                </div>
            </div>
        );
    }
    
}