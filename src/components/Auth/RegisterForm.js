import React from "react";
import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import CardFooter from "@material-tailwind/react/CardFooter";
import InputIcon from "@material-tailwind/react/InputIcon";
import Button from "@material-tailwind/react/Button";
import H5 from "@material-tailwind/react/Heading5";
import Radio from "@material-tailwind/react/radio";
import Textarea from "@material-tailwind/react/Textarea";
const axios = require('axios').default

class RegisterForm extends React.Component {
    constructor(props){
        super(props);
            this.state = {
                first_name: '',
                last_name: '',
                gender: 'M',
                email: 'test@gmail.com',
                password: '',
                confirm_password: '',
                pob: '',
                dob: '',
                tel: '',
                address : '',
                profile: '',
                desc: '',
                errorFirstName: null,
                errorLastName: null,
                errorEmail:null,
                errorPassword:null,
                errorConfirmPassword:null
            };

    }

    componentDidMount() {      

    }

    handleChange = event => {
        const { name, value } = event.target  //Destructure the current fields name and value
        this.setState({ [name]: value });  //Sets state
        event.preventDefault();
    };

    onChangeGender = e => {
        this.setState({ gender: e.target.value});
    }

    isValidEmail(email){
        let lastAtPos = this.state.email.lastIndexOf("@");
        let lastDotPos = this.state.email.lastIndexOf(".");
        if (
                !(
                    lastAtPos < lastDotPos &&
                    lastAtPos > 0 &&
                    this.state.email.indexOf("@@") == -1 &&
                    lastDotPos > 2 &&
                    this.state.email.length - lastDotPos > 2
                )
            ){
                return false;

            }else{
                return true;
            }

    }
    

    handleSubmit = event => {

        //event.preventDefault();

        // Validation Form

        // Firstname
        if(this.state.first_name == '') {
            this.setState({errorFirstName: "First Name is required"});
        }else {
            this.setState({errorFirstName: null});
        }

        // Lastname
        if(this.state.last_name == '') {
            this.setState({errorLastName: "Last Name is required"});
        } else {
            this.setState({errorLastName: null});
        }

        // Email 
        if(this.state.email == '') {
            this.setState({errorEmail: "Email is required"});
            
        }else if(!(this.isValidEmail(this.state.email))){
            
                this.setState({errorEmail: "Email is invalid"});
           
        }else{
            this.setState({errorEmail: null})
        }
        
        
       
        // Paasword
        if(this.state.password == ''){
            this.setState({errorPassword: "Password is required"});
        }else if(this.state.password.length < 8){
            this.setState({errorPassword:"Password must be more than 8 characters"})
        }else{
            this.setState({errorPassword:null})
        }
        
        
        // Confirm password
        if(this.state.confirm_password == '') {
            this.setState({errorConfirmPassword: "Confirm Password is required"});
        }else if(this.state.password !== this.state.confirm_password){
            this.setState({errorConfirmPassword: "Password not match !!"});
        }else{
            this.setState({errorConfirmPassword:null})
        }
            
        // 
        let fields = this.state;

        if(fields.first_name !== "" && fields.last_name !== "" 
            && fields.email !== ""  && this.isValidEmail(fields.email)
            && fields.password !=="" && fields.password.length >7 
            && fields.confirm_password !== "" && fields.password == fields.confirm_password){

                //Alter your Axios request like below
                let params = {
                    first_name: this.state.first_name,
                    last_name: this.state.last_name,
                    gender: this.state.gender,
                    email: this.state.email,
                    password: this.state.password,
                // address: this.state.address,
                // pob: this.state.pob,
                    // dob: this.state.dob,
                    // tel: this.state.tel,
                    // desc: this.state.desc,
                    // profile: this.status.profile,
                    // status: this.state.status
            }
            console.log("data input :",params);

            
            axios({
                method: 'post',
                url: 'http://localhost:5000/add_user',
                // headers: {
                //     'crossDomain': true,  //For cors errors 
                //     'Content-Type': 'application/x-www-form-urlencoded'
                // },
                data: params
            }).then(res => {
                console.log(res);
                console.log(res.data);
            }).catch(err => {
                console.log("error:",err)
            })
        }

    } 
       

    render() {
        return (
      
            <Card className="w-96 mx-auto">
                <CardHeader color="lightBlue" size="lg">
                    <H5 color="white">Register</H5>
                </CardHeader>

                <CardBody>
                    <div className="mt-4 mb-8 px-4">
                        <InputIcon
                            type="text"
                            color="lightBlue"
                            placeholder="First Name"
                            iconName="account_circle"
                            id="first_name" 
                            name="first_name" 
                            value={this.state.first_name}
                            error = {this.state.errorFirstName}
                            onChange={(e) => this.setState({first_name: e.target.value})}

                        />
                        <br />
                    </div>
                    <div className="mt-4 mb-8 px-4">
                        <InputIcon
                            type="text"
                            color="lightBlue"
                            placeholder="Last Name"
                            iconName="account_circle"
                            id="last_name" 
                            name="last_name" 
                            value={this.state.last_name}
                            error = {this.state.errorLastName}
                            onChange={(e) => this.setState({last_name: e.target.value})}
                        />
                      
                    </div>
                    <div className="mt-4 mb-8 px-4">
                        <InputIcon
                            type="email"
                            color="lightBlue"
                            iconName="email"
                            id="email" 
                            name="email" 
                            value={this.state.email}
                            error = {this.state.errorEmail}
                            onChange={(e) => this.setState({email: e.target.value})}
                        />
                       
                    </div>
                    <div className="mt-4 mb-8 px-4">
                        <InputIcon
                            type="password"
                            color="lightBlue"
                            placeholder="password"
                            iconName="lock"
                            id="password" 
                            name="password" 
                            placeholder="Enter password" 
                            value={this.state.password}
                            error = {this.state.errorPassword}
                            onChange={(e) => this.setState({password: e.target.value})}
                           
                        />
                      
                    </div>

                    <div className="mt-4 mb-8 px-4">
                        <InputIcon
                            type="password"
                            color="lightBlue"
                            placeholder="Enter confirm password" 
                            id="confirm_password"
                            iconName="lock"
                            name="confirm_password" 
                            placeholder="Enter confirm password" 
                            value={this.state.confirm_password}
                            error = {this.state.errorConfirmPassword}
                            onChange={(e) => this.setState({confirm_password: e.target.value})}

                        />
                    </div>
                    

                    {/* <div calssName="mb-4 px-4">
                    <Textarea
                        color="lightBlue"
                        size="regular"
                        outline={false}
                        placeholder="Please Input Place Of Birth"
                        id="pob" 
                        name="pob" 
                        value={this.state.pob}
                        onChange={this.handleChange}
                    />
                    </div> */}
                    <div calssName="mb-4 px-4">
                    <Radio
                        color="lightBlue"
                        text="Male"
                        id="M"
                        name="gender"
                        value="M"
                      
                        checked={this.state.gender === "M"}
                        onChange={this.onChangeGender}
                        
                    />
                    <Radio
                        color="lightBlue"
                        text="Female"
                        id="F"
                        name="gender"
                        value="F"
                       checked={this.state.gender === "F"}
                       onChange={this.onChangeGender}
                    />
                    </div>
                </CardBody>
                <CardFooter>
                    <div className="flex justify-center">
                        <Button
                            color="lightBlue"
                            ripple="light"
                            buttonType="link"
                            size="lg"
                            ripple="dark"
                            onClick={this.handleSubmit}
                        >
                            Register
                        </Button>
                    </div>
                </CardFooter>
            </Card>
     
        );
    }
}

export default RegisterForm;